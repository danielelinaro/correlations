#!/usr/bin/env python

from neuron import h
import nrnutils as nrn
import h5utils as h5
import lcg
import numpy as np
import pylab as p

def simulate(segment, R_exc, R_inh):
    ratio = R_exc / R_inh
    mu_ampa,mu_gaba,sigma_ampa,sigma_gaba = lcg.computeSynapticBackgroundCoefficients(ratio, R_exc, N=10)
    dur = 10000
    dt = 5e-3
    delay = 100
    ampa = nrn.makeNoisyGclamp(segment, dur, dt, mu_ampa, sigma_ampa, 5, 0, delay, seed=int(np.random.uniform(high=1000000)))
    gaba = nrn.makeNoisyGclamp(segment, dur, dt, mu_gaba, sigma_gaba, 10, -80, delay, seed=int(np.random.uniform(high=1000000)))
    apc = nrn.makeAPcount(segment)
    rec = nrn.makeRecorders(segment, {'v':'_ref_v'})
    rec['spks'] = h.Vector()
    apc.record(rec['spks'])
    nrn.run(dur+2*delay, dt)
    if apc.n == 0:
        cv = 0
        f = 0
    else:
        isi = np.diff(rec['spks'])
        cv = np.std(isi)/np.mean(isi)
        f = apc.n/(dur*1e-3)

    p.figure()
    p.plot(rec['t'],rec['v'],'k')
    p.title('F = %.1f Hz - CV = %.2f' % (f,cv))
    p.show()

    return f,cv

length = 80                     # [um]
diam = 80                       # [um]
n = nrn.makeRS(length,diam)
Rm = 1e2 / (n(0.5).pas.g * length*diam*np.pi) # [MOhm]

print('')
print('        Cell properties        ')
print('-------------------------------')
print('  length = %.0f um' % length)
print('  diameter = %.0f um' % diam)
print('  input resistance = %3.0f MOhm' % Rm)
print('')

h.celsius = 35
F,CV = simulate(n(0.5), 15000, 3000)
