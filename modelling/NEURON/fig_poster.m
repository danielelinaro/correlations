clear all;
close all;
clc;

%%
load 05/c_vs_rate_gclamp.mat

edges = 0:25;
[cnt,bin] = histc(geom_mean_rate,edges);

x = edges(1:end-1) + diff(edges(1:2))/2;
m = zeros(length(edges)-1,1);
s = zeros(length(edges)-1,1);
for k=1:length(edges)-1
    m(k) = mean(rho(bin==k));
    s(k) = std(rho(bin==k));% / sqrt(sum(bin==edges(k)));
end

[R,P] = corrcoef([CV_pair,rho']);

figure;
hold on;
plot(geom_mean_rate, rho, 'o', 'Color',[.6,.6,.6],...
    'MarkerFaceColor',[.6,.6,.6],'MarkerSize',4);
errorbar(x,m,s,'ks-','LineWidth',2);
plot([0,15],0.5+[0,0],'r--','LineWidth',2);
text(2,0.45,sprintf('N_{cells}=%d',ncells));
text(2,0.4,sprintf('N_{pairs}=%d',length(rho)));
xlabel('Geometric mean output rate (AP/s)');
ylabel('Output correlation \rho');
axis([0,15,0,0.5]);
set(gca,'TickDir','Out','XTick',0:5:15,'YTick',0:.1:.5);
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,7,4]);
print('-depsc2','spikes_corr.eps');

figure;
hold on;
cmap = jet(ncells);
plot([0,15],[1,1],'--','Color',[.6,.6,.6],'LineWidth',2);
for i=1:ncells
    plot(NU{i},CV{i},'o','Color',cmap(i,:),'MarkerFaceColor',cmap(i,:),...
        'MarkerSize',6);
end
axis([0,15,0.5,1.1]);
set(gca,'XTick',0:5:15,'YTick',0:0.5:1,'TickDir','Out');
xlabel('\nu_i (AP/s)');
ylabel('CV_i');
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,7,3]);
print('-depsc2','cv.eps');

%%
load 02/c_vs_rate.mat
figure;
hold on;
plot(geom_mean_rate,C_Itrimmed,'ko');
plot([0,15],0.5+[0,0],'r--','LineWidth',2);
xlabel('Geometric mean rate (AP/s)');
ylabel('Current correlation');
text(10,0.46,sprintf('N_{cells}=%d',ncells));
text(10,0.44,sprintf('N_{pairs}=%d',length(C)));
axis([0,15,0.3,0.5]);
set(gca,'XTick',0:5:15,'YTick',0.2:0.1:0.5,'TickDir','Out');
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,7,3]);
print('-depsc2','current_corr.eps');

%%

gclamp = load('05/c_vs_rate_gclamp.mat','geom_mean_rate','rho');
cclamp = load('06/c_vs_rate_cclamp.mat','geom_mean_rate','rho');

figure;
hold on;
plot(cclamp.geom_mean_rate,cclamp.rho,'ko');
plot(gclamp.geom_mean_rate,gclamp.rho,'r.');
legend('Current clamp','Conductance clamp','Location','SouthEast');
xlabel('Geometric mean rate (AP/s)');
ylabel('\rho');
axis([0,15,0,0.5]);
set(gca,'XTick',0:5:15,'YTick',0:.1:.5);
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,8,6]);
print('-depsc2','gclamp_vs_cclamp.eps');
