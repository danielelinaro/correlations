#!/usr/bin/env python

from neuron import h
import nrnutils as nrn
import lcg
import numpy as np
import numpy.random as rnd
import scipy.io as io
import os
import random

import pylab as p

def run_block(neuron_props, bg_props, simulation_props):
    n = nrn.makeRS(neuron_props['length'],neuron_props['diam'])
    seeds = {'independent': [random.randint(1,100000) for i in range(simulation_props['ntrials'])]}
    np.random.seed(5061983)
    seeds['common'] = map(int, np.random.uniform(low=0, high=10000, size=simulation_props['ntrials']))
    h.celsius = 36
    I = {'mu': bg_props['mu'], 'sigma': np.sqrt(1-bg_props['c'])*bg_props['sigma'], 'sigma_c': np.sqrt(bg_props['c'])*bg_props['sigma']}
    spikeTimes = []
    for s,sc in zip(seeds['independent'],seeds['common']):
        I['stim'],I['vec'] = nrn.makeNoisyIclamp(n(0.5), simulation_props['tstim'], h.dt, I['mu'], I['sigma'], \
                                                 bg_props['tau'], delay=simulation_props['tpre'], seed=s)
        I['stim_c'],I['vec_c'] = nrn.makeNoisyIclamp(n(0.5), simulation_props['tstim'], h.dt, 0, I['sigma_c'], \
                                                 bg_props['tau'], delay=simulation_props['tpre'], seed=sc)
        # recorders
        apc = nrn.makeAPcount(n(0.5))
        spikes = h.Vector()
        apc.record(spikes)
        rec = nrn.makeRecorders(n(0.5),{'v':'_ref_v'})
        # run the simulation
        nrn.run(simulation_props['tstim']+simulation_props['tpre']+simulation_props['tpost'])
        p.plot(rec['t'],rec['v'],'k')
        p.show()
        spikeTimes.append((np.array(spikes)*1e-3).tolist())
        print('Frequency: %g Hz.' % (apc.n*1000/(simulation_props['tstim']-simulation_props['tpre'])))
    return spikeTimes,seeds

def binarize_spike_train(spks,bin_width,interval,T):
    edges = np.arange(interval[0],interval[1]+bin_width/2,bin_width)
    nedges = len(edges)
    y = np.double(np.array(map(lambda x: np.histogram(x,np.append(edges,edges[-1]+bin_width))[0], spks)))
    window = np.round(T/bin_width)
    n = np.zeros((len(spks),nedges-window))
    for i in range(int(nedges-window)):
        n[:,i] = np.sum(y[:,i:i+window],axis=1)
    return y,edges,n

def analyse_spike_times(output_dir, spikes, t0, dur, ttran=0.1, bin_width=1.2e-3, window_size=40e-3):
    offset = 0
    isi = np.array([])
    for k,trial in enumerate(spikes):
        trial = np.array(trial)
        trial = trial[np.nonzero(trial >= (t0+ttran))[0]] - (t0+ttran)
        spikes[k] = trial.tolist()
        if len(trial) == 0:
            offset = offset+dur-ttran
        else:
            try:
                if len(isi) == 0:
                    isi = offset + trial[0]
                else:
                    isi = np.append(isi, trial[0] + offset + dur - ttran - last)
            except:
                isi = np.array([isi, trial[0] + offset + dur - ttran - last])
            last = trial[0]
            if len(trial) > 1:
                tmp = np.diff(trial)
                for i in tmp:
                    isi = np.append(isi,i)
                last = trial[-1]
            offset = 0
    rates = map(lambda x: len(x)/(dur-ttran), spikes)
    y,edges,n = binarize_spike_train(spikes,bin_width,[0,dur-ttran],window_size)
    cv = np.std(isi)/np.mean(isi)
    nu = np.mean(rates)
    matlab_data = {'L': dur-ttran, 'T': window_size, 'binWidth': bin_width,
                   'cv': cv, 'dur': dur, 'edges': edges, 'isi': isi, 'n': n,
                   'nu': nu, 'rates': rates, 'spiketimes': spikes,
                   't0': t0, 'ttran': ttran, 'windowSize': window_size, 'y': y}
    io.savemat(output_dir + '/correlations.mat',matlab_data,do_compression=True,oned_as='column')
              
def main():
    neuron_props = {'length': 80, 'diam': 80}
    bg_props = {'c': 0.5, 'tau': 5}
    simulation_props = {'tstim': 5100., 'tpre': 100., 'tpost': 100., 'ntrials': 1, 'ttran': 100.}
    n_currents = 5
    cell_no = 1
    while os.path.isdir('cell_%03d' % cell_no):
        cell_no += 1
    os.mkdir('cell_%03d' % cell_no, 0755)
    os.mkdir('cell_%03d/correlations' % cell_no, 0755)
    io.savemat('cell_%03d/neuron.mat' % cell_no, neuron_props, do_compression=True, oned_as='column')
    io.savemat('cell_%03d/background.mat' % cell_no, bg_props, do_compression=True, oned_as='column')
    io.savemat('cell_%03d/simulation.mat' % cell_no, simulation_props, do_compression=True, oned_as='column')
    mu = [0.35,0.45]
    for i,m in enumerate(mu):
        sigma = rnd.uniform(0.05,0.25,n_currents)
        for j,s in enumerate(sigma):
            bg_props['mu'] = m
            bg_props['sigma'] = s
            print('Simulating cell with mu = %g pA and sigma = %g pA.' % (m*1e3,s*1e3))
            spikeTimes,seeds = run_block(neuron_props, bg_props, simulation_props)
            dirname = 'cell_%03d/correlations/%02d' % (cell_no,i*len(sigma)+j+1)
            os.mkdir(dirname)
            io.savemat(dirname + '/misc.mat', seeds, do_compression=True, oned_as='column')
            print('Analysing data.')
            analyse_spike_times(dirname, spikeTimes, simulation_props['tpre']*1e-3, \
                                    simulation_props['tstim']*1e-3, simulation_props['ttran']*1e-3)

if __name__ == '__main__':
    main()
