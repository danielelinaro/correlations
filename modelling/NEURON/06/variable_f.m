clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary;

%% 
% Compute spike train correlation as a function of the geometric mean
% firing rate of the two cells.
cells = arrayfun(@(x) x.name, dir('cell_0*'), 'UniformOutput', 0);
ncells = length(cells);
delta = 30e-3;

NU = cell(ncells,1);
CV = cell(ncells,1);
C = cell(ncells,1);

geom_mean_rate = [];
rho = [];
CV_pair = [];
for i=1:ncells
    stimulus_sets_i = listDirectories([cells{i},'/correlations']);
    N{i} = zeros(length(stimulus_sets_i),1);
    CV{i} = zeros(length(stimulus_sets_i),1);
    if isempty(C{i})
        C{i} = nan(length(stimulus_sets_i),1);
    end
    for n=1:length(stimulus_sets_i)
        data_in = load([cells{i},'/correlations/',...
            stimulus_sets_i{n},'/correlations.mat'],'y','nu','cv','L','T','binWidth');
        NU{i}(n) = data_in.nu;
        CV{i}(n) = data_in.cv;
        for j=1:ncells
            stimulus_sets_j = listDirectories([cells{j},'/correlations']);
            if isempty(C{j})
                C{j} = nan(length(stimulus_sets_j),1);
            end
            for m=1:length(stimulus_sets_j)
                if i == j && m == n
                    continue;
                end
                data_jm = load([cells{j},'/correlations/',...
                    stimulus_sets_j{m},'/correlations.mat'],'y','nu','cv');
                if data_in.nu/data_jm.nu >= 0.5 && data_in.nu/data_jm.nu <= 2
                    fprintf(1, '.');
                    geom_mean_rate = [geom_mean_rate ; sqrt(data_in.nu*data_jm.nu)];
                    CV_pair = [CV_pair ; sqrt(data_in.cv*data_jm.cv)];
                    [r,~,C{i}(n),C{j}(m)] = computeRho(data_in.y,data_jm.y,data_in.L,...
                                    data_in.binWidth,delta,data_in.nu,data_jm.nu,C{i}(n),C{j}(m));
                    rho = [rho, r];
                    if mod(length(rho),50) == 0
                        fprintf(1, '\n');
                    end
                end
            end
        end
    end
end
fprintf(1, '\n');
rho = abs(rho);
save('c_vs_rate_cclamp.mat');

%%
edges = 0:25;
[cnt,bin] = histc(geom_mean_rate,edges);

x = edges(1:end-1) + diff(edges(1:2))/2;
m = zeros(length(edges)-1,1);
s = zeros(length(edges)-1,1);
for k=1:length(edges)-1
    m(k) = mean(rho(bin==k));
    s(k) = std(rho(bin==k));% / sqrt(sum(bin==edges(k)));
end

[R,P] = corrcoef([CV_pair,rho]);

%%
figure;
axes('Position',[0.15,0.1,0.7,0.55],'NextPlot','Add');
plot(geom_mean_rate, rho, 'o', 'Color',[.6,.6,.6],...
    'MarkerFaceColor',[.6,.6,.6],'MarkerSize',4);
errorbar(x,m,s,'ks-','LineWidth',2);
text(2,0.45,sprintf('N_{cells}=%d',ncells));
text(2,0.4,sprintf('N_{pairs}=%d',length(rho)));
xlabel('Geometric mean output rate (AP/s)');
ylabel('Output correlation \rho_T');
axis([0,22,0,0.5]);
set(gca,'TickDir','Out');

axes('Position',[0.15,0.1,0.7,0.1],'NextPlot','Add');
bar(x,cnt(1:end-1),.8,'FaceColor',[.6,.6,1],'EdgeColor','None');
lim = ceil(max(cnt)/10)*10;
set(gca,'YAxisLocation','Right','YTick',0:lim/2:lim,'TickDir','Out');
axis([0,22,0,lim]);
ylabel('Number of pairs');

axes('Position',[0.15,0.75,0.3,0.2],'NextPlot','Add');
cmap = jet(ncells);
plot([0,22],[1,1],'--','Color',[.6,.6,.6],'LineWidth',2);
for i=1:ncells
    plot(NU{i},CV{i},'o','Color',cmap(i,:),'MarkerFaceColor',cmap(i,:),...
        'MarkerSize',6);
end
axis([0,22,0.3,1.1]);
set(gca,'XTick',0:10:20,'YTick',0:0.5:1,'TickDir','Out');
xlabel('\nu_i (AP/s)');
ylabel('CV_i');

axes('Position',[0.55,0.75,0.3,0.2],'NextPlot','Add');
plot(CV_pair,rho,'o', 'Color',[.6,.6,.6],...
    'MarkerFaceColor',[.6,.6,.6],'MarkerSize',4);
plot([0.3,1],polyval(polyfit(CV_pair,rho,1),[0.3,1]),'k','LineWidth',2)
title(sprintf('R = %.3f - p = %.1f', R(2), P(2)));
axis([0.3,1,0,0.5]);
xlabel('Sqrt(CV_i CV_j)');
ylabel('\rho_T');
set(gca,'TickDir','Out');

set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,7,5]);
print('-depsc2','c_vs_rate_cclamp.eps');
