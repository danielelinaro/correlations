#!/usr/bin/env python

from neuron import h
import nrnutils as nrn
import lcg
import numpy as np
import httplib as http
import scipy.io as io
import os
import random
import sys

def run_block(neuron_props, bg_props, simulation_props, min_no_spikes=2000):
    n = nrn.makeRS(neuron_props['length'],neuron_props['diam'])
    area = np.pi*neuron_props['length']*neuron_props['diam']*1e-8       # [cm^2]
    G = n(0.5).pas.g * area * 1e9  # [nS]
    Rm = 1/(G*1e-3)                # [MOhm}

    #print('Length = %.3f um, Diameter = %.3f um, Input resistance = %.3f MOhm' \
    #    % (neuron_props['length'],neuron_props['diam'],Rm))

    np.random.seed(5061983)
    seeds = {'ampa_common': map(int, np.random.uniform(low=0, high=100000, size=simulation_props['ntrials']))}
    np.random.seed(7051983)
    seeds['gaba_common'] = map(int, np.random.uniform(low=0, high=100000, size=simulation_props['ntrials']))

    h.celsius = 36
    ratio = lcg.computeRatesRatio(Vm=bg_props['Vbal'], Rin=Rm)

    gexc = {'mu': 0., 'sigma': 0., 'mu_c': 0., 'sigma_c': 0., 'tau': 5, 'E': 0}
    ginh = {'mu': 0., 'sigma': 0., 'mu_c': 0., 'sigma_c': 0., 'tau': 10, 'E': -80}

    gexc['mu'],ginh['mu'],gexc['sigma'],ginh['sigma'] = \
        lcg.computeSynapticBackgroundCoefficients(ratio, bg_props['R_exc']*(1-bg_props['c']), Rin=Rm)
    gexc['mu_c'],ginh['mu_c'],gexc['sigma_c'],ginh['sigma_c'] = \
        lcg.computeSynapticBackgroundCoefficients(ratio, bg_props['R_exc']*bg_props['c'], Rin=Rm)

    spikeTimes = []

    #N = np.round((simulation_props['tstim']+simulation_props['tpre']+simulation_props['tpost'])/h.dt) + 1
    #V = np.zeros((simulation_props['ntrials'],N))
    #I = np.zeros((simulation_props['ntrials'],N))
    #i = 0
    nspikes = 0
    block_no = 1
    seeds['ampa'] = []
    seeds['gaba'] = []
    while nspikes < min_no_spikes:
        spikes = []
        for seed_exc,seed_inh in zip(seeds['ampa_common'],seeds['gaba_common']):
            seeds['ampa'].append(random.randint(1,100000))
            seeds['gaba'].append(random.randint(1,100000))
            gexc['stim'],gexc['vec'] = nrn.makeNoisyGclamp(n(0.5), simulation_props['tstim'], h.dt, gexc['mu'], \
                                                               gexc['sigma'], gexc['tau'], gexc['E'], \
                                                               delay=simulation_props['tpre'], seed=seeds['ampa'][-1])
            ginh['stim'],ginh['vec'] = nrn.makeNoisyGclamp(n(0.5), simulation_props['tstim'], h.dt, ginh['mu'], \
                                                               ginh['sigma'], ginh['tau'], ginh['E'], \
                                                               delay=simulation_props['tpre'], seed=seeds['gaba'][-1])
            gexc['stim_c'],gexc['vec_c'] = nrn.makeNoisyGclamp(n(0.5), simulation_props['tstim'], h.dt, gexc['mu_c'], \
                                                                   gexc['sigma_c'], gexc['tau'], gexc['E'], \
                                                                   delay=simulation_props['tpre'], seed=seed_exc)
            ginh['stim_c'],ginh['vec_c'] = nrn.makeNoisyGclamp(n(0.5), simulation_props['tstim'], h.dt, ginh['mu_c'], \
                                                                   ginh['sigma_c'], ginh['tau'], ginh['E'], \
                                                                   delay=simulation_props['tpre'], seed=seed_inh)
            # recorders
            apc = nrn.makeAPcount(n(0.5))
            spks = h.Vector()
            apc.record(spks)
            #rec = nrn.makeRecorders(n(0.5),{'Vm': '_ref_v'})
            #rec = nrn.makeRecorders(gexc['stim'],{'i_exc': '_ref_i'},rec)
            #rec = nrn.makeRecorders(ginh['stim'],{'i_inh': '_ref_i'},rec)
            #rec = nrn.makeRecorders(gexc['stim_c'],{'i_exc_c': '_ref_i'},rec)
            #rec = nrn.makeRecorders(ginh['stim_c'],{'i_inh_c': '_ref_i'},rec)
            # run the simulation
            nrn.run(simulation_props['tstim']+simulation_props['tpre']+simulation_props['tpost'])
            nspikes += apc.n
            spikes.append((np.array(spks)*1e-3).tolist())
            #V[i,:] = np.array(rec['Vm'])
            #I[i,:] = np.array(rec['i_exc'])+np.array(rec['i_inh'])+np.array(rec['i_exc_c'])+np.array(rec['i_inh_c'])
            #i += 1
        if block_no == 1 and nspikes < simulation_props['ntrials']*(simulation_props['tstim']-simulation_props['ttran'])*1e-3:
            # only frequencies >= 1 Hz
            return None,seeds
        spikeTimes.append(spikes)
        block_no += 1
    return spikeTimes,seeds#,V,I

def binarize_spike_train(spks,bin_width,interval,T):
    edges = np.arange(interval[0],interval[1]+bin_width/2,bin_width)
    nedges = len(edges)
    y = np.double(np.array(map(lambda x: np.histogram(x,np.append(edges,interval[1]))[0], spks)))
    window = np.round(T/bin_width)
    n = np.zeros((len(spks),nedges-window))
    for i in range(int(nedges-window)):
        n[:,i] = np.sum(y[:,i:i+window],axis=1)
    return y,edges,n

def analyse_spike_times(output_dir, spikes, t0, dur, ttran=0.1, bin_width=1.2e-3, window_size=40e-3):
    offset = 0
    isi = np.array([])
    y = []
    edges = []
    n = []
    rates = []
    for block in spikes:
        for i,trial in enumerate(block):
            trial = np.array(trial)
            trial = trial[np.nonzero(trial >= (t0+ttran))[0]] - (t0+ttran)
            block[i] = trial.tolist()
            if len(trial) == 0:
                offset = offset+dur-ttran
            else:
                try:
                    if len(isi) == 0:
                        isi = offset + trial[0]
                    else:
                        isi = np.append(isi, trial[0] + offset + dur - ttran - last)
                except:
                    isi = np.array([isi, trial[0] + offset + dur - ttran - last])
                last = trial[0]
                if len(trial) > 1:
                    tmp = np.diff(trial)
                    for i in tmp:
                        isi = np.append(isi,i)
                    last = trial[-1]
                offset = 0
        ret = binarize_spike_train(block,bin_width,[0,dur-ttran],window_size)
        y.append(ret[0])
        edges.append(ret[1])
        n.append(ret[2])
        rates.append(map(lambda x: len(x)/(dur-ttran), block))
    cv = np.std(isi)/np.mean(isi)
    nu = np.mean(rates)
    matlab_data = {'L': dur-ttran, 'T': window_size, 'binWidth': bin_width,
                   'cv': cv, 'dur': dur, 'edges': edges, 'isi': isi, 'n': n,
                   'nu': nu, 'rates': rates, 'spiketimes': spikes,
                   't0': t0, 'ttran': ttran, 'windowSize': window_size, 'y': y,
                   'Ee': 0., 'Ei': -80.}
    io.savemat(output_dir + '/correlations.mat',matlab_data,do_compression=True,oned_as='column')

def main():
    ncells = 5
    np.random.seed(random.randint(1,10000))
    length = np.random.normal(80,20,ncells)    # [um]
    diam = np.random.normal(80,20,ncells)      # [um]
    n_balanced_voltages = 10
    bg_props = {'R_exc': 7000, 'c': 0.5}
    simulation_props = {'tstim': 1100, 'tpre': 100, 'tpost': 100, 'ttran': 100, 'ntrials': 100}
    cell_no = 1
    while os.path.isdir('cell_%03d' % cell_no):
        cell_no += 1
    import pylab as p
    for L,D in zip(length,diam):
        neuron_props = {'length': L, 'diam': D}
        os.mkdir('cell_%03d' % cell_no, 0755)
        os.mkdir('cell_%03d/correlations' % cell_no, 0755)
        i = 0
        print('Cell # %02d/%02d.' % (cell_no,ncells))
        while i < n_balanced_voltages:
            bg_props['Vbal'] = -42.+32.*random.random()
            sys.stdout.write('\tVoltage # %02d/%02d (%.2f mV)... ' % (i+1,n_balanced_voltages,bg_props['Vbal']))
            sys.stdout.flush()
            #spikeTimes,Vm,I = run_block(neuron_props, bg_props, simulation_props)
            spikeTimes,seeds = run_block(neuron_props, bg_props, simulation_props, 2500)
            if not spikeTimes:
                sys.stdout.write('frequency too low, repeating.\n')
                continue
            sys.stdout.write('ok.\n')
            dirname = 'cell_%03d/correlations/%02d' % (cell_no,i+1)
            os.mkdir(dirname)
            analyse_spike_times(dirname, spikeTimes, simulation_props['tpre']*1e-3, \
                                    simulation_props['tstim']*1e-3, simulation_props['ttran']*1e-3)

            io.savemat(dirname + '/seeds.mat', seeds, do_compression=True, oned_as='column')

            #t = np.arange(Vm.shape[-1])*h.dt
            #idx = np.intersect1d(np.nonzero(t>=simulation_props['tpre']+ttran)[0],
            #                     np.nonzero(t<=simulation_props['tpre']+simulation_props['tstim'])[0])
            #Vm = Vm[:,idx]
            #I = I[:,idx]
            #Vtrimmed,ndx = nrn.removeSpikes(h.dt,Vm,0,[3,5],in_place=True)
            #Itrimmed = I.copy()
            #for j in range(Itrimmed.shape[0]):
            #    Itrimmed[j,ndx[j]] = np.nan
            #I = I.flatten()
            #Itrimmed = Itrimmed.flatten()
            #nans = np.nonzero(np.isnan(Itrimmed))[0]
            #not_nans = np.setdiff1d(np.arange(len(Itrimmed)),nans)
            #Itrimmed[nans] = np.mean(Itrimmed[not_nans])
            #io.savemat(dirname + '/I.mat',{'dt': h.dt*1e-3, 'I': I*1e3},\
            #               do_compression=True,oned_as='column')
            #io.savemat(dirname + '/Itrimmed.mat',{'dt': h.dt*1e-3, 'Itrimmed': Itrimmed*1e3},\
            #               do_compression=True,oned_as='column')
            i += 1
        cell_no += 1

if __name__ == '__main__':
    main()
