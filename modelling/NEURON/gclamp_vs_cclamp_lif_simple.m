clear all;
close all;
clc;

%%
gclamp = load('05/c_vs_rate_gclamp.mat','geom_mean_rate','rho');
cclamp = load('../Matlab/LIF_simple_correlations_cc_20140523141156.mat',...
    'geom_mean_rate','r');

cclamp.edges = fix(min(cclamp.geom_mean_rate)):100;
[cclamp.n,bin] = histc(cclamp.geom_mean_rate,cclamp.edges);
cclamp.r_m = zeros(length(cclamp.edges)-1,1);
cclamp.r_s = zeros(length(cclamp.edges)-1,1);
for i=1:length(cclamp.r_m)
    cclamp.r_m(i) = mean(cclamp.r(bin==i));
    cclamp.r_s(i) = std(cclamp.r(bin==i));
end
cclamp.edges = cclamp.edges(1:end-1) + diff(cclamp.edges)/2;

gclamp.edges = 0:20;
[gclamp.n,bin] = histc(gclamp.geom_mean_rate,gclamp.edges);
gclamp.r_m = zeros(length(gclamp.edges)-1,1);
gclamp.r_s = zeros(length(gclamp.edges)-1,1);
for i=1:length(gclamp.r_m)
    gclamp.r_m(i) = mean(gclamp.rho(bin==i));
    gclamp.r_s(i) = std(gclamp.rho(bin==i));
end
gclamp.edges = gclamp.edges(1:end-1) + diff(gclamp.edges)/2;

figure;
hold on;
errorbar(gclamp.edges,gclamp.r_m,gclamp.r_s,'ro','MarkerFaceColor','r',...
    'MarkerSize',6);
errorbar(cclamp.edges(cclamp.edges<=20),cclamp.r_m(cclamp.edges<=20),...
    cclamp.r_s(cclamp.edges<=20),'ko','MarkerFaceColor','k',...
    'MarkerSize',6);
axis([0,20,0,0.5]);
h = legend('Conductance clamp','Current clamp','Location','SouthEast');
set(h,'box','off');
set(gca,'XTick',0:5:20,'YTick',0:.1:.5);
xlabel('Geometric mean rate');
ylabel('Correlation');
box off;
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,8,5]);
print('-depsc2','gclamp_vs_cclamp_lif_simple.eps');
