clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary

%% 
% Compute spike train correlation as a function of the geometric mean
% firing rate of the two cells.

cells = {'cell_003','cell_004','cell_005','cell_006','cell_007','cell_008','cell_009','cell_010','cell_011',};
ncells = length(cells);

NU = cell(ncells,1);
CV = cell(ncells,1);
N = cell(ncells,1);

geom_mean_rate = [];
r = [];
CV_pair = [];
c_ii = nan(ncells,1);
bin_width = 2e-3;
delta = 30*bin_width;
for i=1:ncells
    stimulus_sets_i = listDirectories([cells{i},'/correlations']);
    for n=1:length(stimulus_sets_i)
        if exist([cells{i},'/correlations/',...
            stimulus_sets_i{n},'/DO_NOT_USE'],'file')
            continue;
        end
        data_in = load([cells{i},'/correlations/',...
            stimulus_sets_i{n},'/correlations.mat'],'n','nu','cv','L','y','rates');
        NU{i} = [NU{i} ; data_in.nu];
        CV{i} = [CV{i} ; data_in.cv];
        N{i} = [N{i}, sum(sum(data_in.y))];
        [~,~,c_ii(i)] = computeRho(data_in.y,data_in.y,data_in.L,bin_width,delta,data_in.rates,data_in.rates);
        for j=i+1:ncells
            stimulus_sets_j = listDirectories([cells{j},'/correlations']);
            for m=1:length(stimulus_sets_j)
                if (i==j && m==n) || exist([cells{j},'/correlations/',...
                        stimulus_sets_j{m},'/DO_NOT_USE'],'file')
                    continue;
                end
                data_jm = load([cells{j},'/correlations/',...
                    stimulus_sets_j{m},'/correlations.mat'],'n','nu','cv','L','y','rates');
                if isnan(c_ii(j))
                    [~,~,c_ii(j)] = computeRho(data_jm.y,data_jm.y,data_jm.L,bin_width,delta,data_jm.rates,data_jm.rates);
                end
                if data_in.nu/data_jm.nu >= 0.5 && data_in.nu/data_jm.nu <= 2
                    fprintf(1, '.');
                    geom_mean_rate = [geom_mean_rate ; sqrt(data_in.nu*data_jm.nu)];
                    CV_pair = [CV_pair ; sqrt(data_in.cv*data_jm.cv)];
                    r = [r; computeRho(data_in.y,data_jm.y,data_in.L,bin_width,delta,data_in.rates,...
                                data_jm.rates,c_ii(i),c_ii(j))];
                    if mod(length(r),50) == 0
                        fprintf(1, '\n');
                    end
                end
            end
        end
    end
end
fprintf(1, '\n');
clear i j m n
save c_vs_rate.mat

% %%
% edges = 0:25;
% [cnt,bin] = histc(geom_mean_rate,edges);
% 
% x = edges(1:end-1) + diff(edges(1:2))/2;
% m = zeros(length(edges)-1,1);
% s = zeros(length(edges)-1,1);
% for k=1:length(edges)-1
%     m(k) = mean(C(bin==k));
%     s(k) = std(C(bin==k));% / sqrt(sum(bin==edges(k)));
% end
% 
% [R,P] = corrcoef([CV_pair,C]);
% 
% %%
% figure;
% axes('Position',[0.15,0.1,0.7,0.55],'NextPlot','Add');
% plot(geom_mean_rate, C, 'o', 'Color',[.6,.6,.6],...
%     'MarkerFaceColor',[.6,.6,.6],'MarkerSize',4);
% errorbar(x,m,s,'ks-','LineWidth',2);
% text(20,0.45,sprintf('N_{cells}=%d',ncells));
% text(20,0.4,sprintf('N_{pairs}=%d',length(C)));
% xlabel('Geometric mean output rate (AP/s)');
% ylabel('Output correlation \rho_T');
% axis([0,25,0,0.5]);
% set(gca,'TickDir','Out');
% 
% axes('Position',[0.15,0.1,0.7,0.1],'NextPlot','Add');
% bar(x,cnt(1:end-1),.8,'FaceColor',[.6,.6,1],'EdgeColor','None');
% lim = ceil(max(cnt)/10)*10;
% set(gca,'YAxisLocation','Right','YTick',0:lim/2:lim,'TickDir','Out');
% axis([0,25,0,lim]);
% ylabel('Number of pairs');
% 
% axes('Position',[0.15,0.75,0.3,0.2],'NextPlot','Add');
% cmap = jet(ncells);
% plot([0,22],[1,1],'--','Color',[.6,.6,.6],'LineWidth',2);
% for i=1:ncells
%     plot(NU{i},CV{i},'o','Color',cmap(i,:),'MarkerFaceColor',cmap(i,:),...
%         'MarkerSize',6);
% end
% axis([0,22,0.3,1.1]);
% set(gca,'XTick',0:10:20,'YTick',0:0.5:1,'TickDir','Out');
% xlabel('\nu_i (AP/s)');
% ylabel('CV_i');
% 
% axes('Position',[0.55,0.75,0.3,0.2],'NextPlot','Add');
% plot(CV_pair,C,'o', 'Color',[.6,.6,.6],...
%     'MarkerFaceColor',[.6,.6,.6],'MarkerSize',4);
% plot([0.3,1],polyval(polyfit(CV_pair,C,1),[0.3,1]),'k','LineWidth',2)
% title(sprintf('R = %.3f - p = %.1f', R(2), P(2)));
% axis([0.3,1,0,0.5]);
% xlabel('Sqrt(CV_i CV_j)');
% ylabel('\rho_T');
% set(gca,'TickDir','Out');
% 
% set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,7,5]);
% print('-depsc2','c_vs_rate.eps');
% 
% %%
% figure;
% hold on;
% plot(geom_mean_rate,C_Itrimmed,'ko');
% xlabel('Geometric mean rate (AP/s)');
% ylabel('Trimmed current correlation');
% text(20,0.46,sprintf('N_{cells}=%d',ncells));
% text(20,0.44,sprintf('N_{pairs}=%d',length(C)));
% axis([0,25,0.2,0.5]);
% set(gca,'XTick',0:5:25,'YTick',0.2:0.05:0.5);
% set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,6,4]);
% print('-depsc2','c_vs_rate_trimmed_current.eps');
% % 
% % %%
% % cmap = jet(ncells);
% % figure;
% % hold on;
% % for i=1:ncells
% %     [x,idx] = sort(NU{i});
% %     y = Vth{i}(idx)-Vsubthresh{i}(idx);
% %     y = Vsubthresh{i}(idx);
% % %     y = y/y(end);
% %     plot(x,y,'Color',cmap(i,:));
% %     plot(x([1,end]),y([1,end]),'o-','Color',cmap(i,:),'MarkerFaceColor',cmap(i,:));
% % end
