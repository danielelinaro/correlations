#!/usr/bin/env python

from neuron import h
import nrnutils as nrn
import lcg
import numpy as np
import numpy.random as rnd
import scipy.io as io
import os
import random
import time
import datetime
import sys
import pylab as p

def logger(s):
    print(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S') + ' >>   ' + s)

def run_block(neuron_props, bg_props, simulation_props):
    logger('Started block.')
    n = nrn.makeRS(neuron_props['length'],neuron_props['diam'])
    area = np.pi*neuron_props['length']*neuron_props['diam']*1e-8       # [cm^2]
    G = n(0.5).pas.g * area * 1e9  # [nS]
    Rm = 1/(G*1e-3)                # [MOhm}

    seeds = {'ampa': [random.randint(1,100000) for i in range(simulation_props['ntrials'])], \
                 'gaba': [random.randint(1,100000) for i in range(simulation_props['ntrials'])]}
    np.random.seed(5061983)
    seeds['ampa_common'] = map(int, np.random.uniform(low=0, high=10000, size=simulation_props['ntrials']))
    np.random.seed(7051983)
    seeds['gaba_common'] = map(int, np.random.uniform(low=0, high=10000, size=simulation_props['ntrials']))

    h.celsius = 36
    ratio = lcg.computeRatesRatio(Vm=bg_props['Vbal'], Rin=Rm)

    gexc = {'mu': 0., 'sigma': 0., 'mu_c': 0., 'sigma_c': 0., 'tau': 5, 'E': 0}
    ginh = {'mu': 0., 'sigma': 0., 'mu_c': 0., 'sigma_c': 0., 'tau': 10, 'E': -80}

    gexc['mu'],ginh['mu'],gexc['sigma'],ginh['sigma'] = \
        lcg.computeSynapticBackgroundCoefficients(ratio, bg_props['R_exc']*(1-bg_props['c']), Rin=Rm)
    gexc['mu_c'],ginh['mu_c'],gexc['sigma_c'],ginh['sigma_c'] = \
        lcg.computeSynapticBackgroundCoefficients(ratio, bg_props['R_exc']*bg_props['c'], Rin=Rm)

    spikeTimes = []
    for sa,sg,sac,sgc in zip(seeds['ampa'],seeds['gaba'],seeds['ampa_common'],seeds['gaba_common']):
        gexc['stim'],gexc['vec'] = nrn.makeNoisyGclamp(n(0.5), simulation_props['tstim'], h.dt, gexc['mu'], \
                                                           gexc['sigma'], gexc['tau'], gexc['E'], \
                                                           delay=simulation_props['tpre'], seed=sa)
        ginh['stim'],ginh['vec'] = nrn.makeNoisyGclamp(n(0.5), simulation_props['tstim'], h.dt, ginh['mu'], \
                                                           ginh['sigma'], ginh['tau'], ginh['E'], \
                                                           delay=simulation_props['tpre'], seed=sg)
        gexc['stim_c'],gexc['vec_c'] = nrn.makeNoisyGclamp(n(0.5), simulation_props['tstim'], h.dt, gexc['mu_c'], \
                                                           gexc['sigma_c'], gexc['tau'], gexc['E'], \
                                                               delay=simulation_props['tpre'], seed=sac)
        ginh['stim_c'],ginh['vec_c'] = nrn.makeNoisyGclamp(n(0.5), simulation_props['tstim'], h.dt, ginh['mu_c'], \
                                                           ginh['sigma_c'], ginh['tau'], ginh['E'], \
                                                               delay=simulation_props['tpre'], seed=sgc)
        # recorders
        apc = nrn.makeAPcount(n(0.5))
        spikes = h.Vector()
        apc.record(spikes)
        rec = nrn.makeRecorders(n(0.5),{'v':'_ref_v'})
        # run the simulation
        nrn.run(simulation_props['tstim']+simulation_props['tpre']+simulation_props['tpost'])
        p.plot(rec['t'],rec['v'])
        p.show()
        sys.exit(0)
        logger('Frequency: %g Hz.' % (apc.n*1000/simulation_props['tstim']))
        spikeTimes.append((np.array(spikes)*1e-3).tolist())

    return spikeTimes,seeds

def binarize_spike_train(spks,bin_width,interval,T):
    edges = np.arange(interval[0],interval[1]+bin_width/2,bin_width)
    nedges = len(edges)
    y = np.double(np.array(map(lambda x: np.histogram(x,np.append(edges,interval[1]))[0], spks)))
    window = np.round(T/bin_width)
    n = np.zeros((len(spks),nedges-window))
    for i in range(int(nedges-window)):
        n[:,i] = np.sum(y[:,i:i+window],axis=1)
    return y,edges,n

def analyse_spike_times(output_dir, spikes, t0, dur, ttran=0.1, bin_width=1.2e-3, window_size=40e-3):
    offset = 0
    isi = np.array([])
    for k,trial in enumerate(spikes):
        trial = np.array(trial)
        trial = trial[np.nonzero(trial >= (t0+ttran))[0]] - (t0+ttran)
        spikes[k] = trial.tolist()
        if len(trial) == 0:
            offset = offset+dur-ttran
        else:
            try:
                if len(isi) == 0:
                    isi = offset + trial[0]
                else:
                    isi = np.append(isi, trial[0] + offset + dur - ttran - last)
            except:
                isi = np.array([isi, trial[0] + offset + dur - ttran - last])
            last = trial[0]
            if len(trial) > 1:
                tmp = np.diff(trial)
                for i in tmp:
                    isi = np.append(isi,i)
                last = trial[-1]
            offset = 0
    rates = map(lambda x: len(x)/(dur-ttran), spikes)
    y,edges,n = binarize_spike_train(spikes,bin_width,[0,dur-ttran],window_size)
    cv = np.std(isi)/np.mean(isi)
    nu = np.mean(rates)
    matlab_data = {'L': dur-ttran, 'T': window_size, 'binWidth': bin_width,
                   'cv': cv, 'dur': dur, 'edges': edges, 'isi': isi, 'n': n,
                   'nu': nu, 'rates': rates, 'spiketimes': spikes,
                   't0': t0, 'ttran': ttran, 'windowSize': window_size, 'y': y,
                   'Ee': 0., 'Ei': -80.}
    io.savemat(output_dir + '/correlations.mat',matlab_data,do_compression=True,oned_as='column')

def main():
    neuron_props = {'length': random.normalvariate(80,10), 'diam': random.normalvariate(80,10)}
    bg_props = {'R_exc': 14000., 'c': 0.5}
    #simulation_props = {'tstim': 100100., 'tpre': 100., 'tpost': 100., 'ntrials': 100, 'ttran': 100.}
    simulation_props = {'tstim': 1100., 'tpre': 100., 'tpost': 100., 'ntrials': 100, 'ttran': 100.}
    n_balanced_voltages = 20
    cell_no = 1
    while os.path.isdir('cell_%03d' % cell_no):
        cell_no += 1
    os.mkdir('cell_%03d' % cell_no, 0755)
    os.mkdir('cell_%03d/correlations' % cell_no, 0755)
    io.savemat('cell_%03d/neuron.mat' % cell_no, neuron_props, do_compression=True, oned_as='column')
    io.savemat('cell_%03d/background.mat' % cell_no, bg_props, do_compression=True, oned_as='column')
    io.savemat('cell_%03d/simulation.mat' % cell_no, simulation_props, do_compression=True, oned_as='column')
    Vbal = -60. + np.logspace(np.log10(10),np.log10(55),n_balanced_voltages) + rnd.uniform(-1,1,n_balanced_voltages) # [-50,-5] mV
    Vbal = [-5]
    for i,V in enumerate(Vbal):
        bg_props['Vbal'] = V
        logger('Simulating cell with Vbal = %g mV.' % V)
        spikeTimes,seeds = run_block(neuron_props, bg_props, simulation_props)
        dirname = 'cell_%03d/correlations/%02d' % (cell_no,i+1)
        os.mkdir(dirname)
        io.savemat(dirname + '/misc.mat', seeds, do_compression=True, oned_as='column')
        logger('Analysing data.')
        analyse_spike_times(dirname, spikeTimes, simulation_props['tpre']*1e-3, \
                                simulation_props['tstim']*1e-3, simulation_props['ttran']*1e-3)

if __name__ == '__main__':
    main()
