#!/bin/bash

ncores=4
ncells=8
for (( i=0; i<$ncells; i+=$ncores ))
do
    for (( j=0; j<$ncores; j++ ))
    do
	let n=$i+$j+1
	logfile=cell_$n.log
	./correlations_gclamp.py > $logfile &
	sleep 5
    done
    wait
done
matlab -nojvm -nosplash < variable_f.m > variable_f.log
