function pars = make_neuron_model_pars(model_name)

if strcmpi(model_name,'LIF')
    pars = struct( ...
    'C', 0.08, ...                   % [nF]
    'tau', 0.0075, ...               % [s] default value, gives an input resistance of 93.75 MOhm
    'tarp', 0.0014, ...              % [s]
    'Er', -65.2, ...                 % [mV]
    'E0', -70, ...                   % [mV]
    'Vth', -50);                     % [mV]
    pars.Rm = pars.tau/pars.C*1e3;   % [mOhm]
elseif strcmpi(model_name,'aEIF')
    pars = struct( ...
    'C', 281, ...                    % [pF] membrane capacitance
    'gL', 30, ...                    % [nS] leak conductance
    'EL', -70.6, ...                 % [mV] leak reversal potential
    'VT', -50.4, ...                 % [mV] spike threshold
    'deltaT', 2, ...                 % [mV] slope factor
    'tauw', 0.144, ...               % [s]  adaptation time constant
    'a', 4, ...                      % [nS] subthreshold adaptation
    'b', 0.0805, ...                 % [nA] spike-triggered adaptation
    'Vr', -55, ...                   % [mV] reset voltage
    'tarp', 2e-3);                   % [s]  refractory period
    pars.Rm = 1e3/pars.gL;           % [MOhm]
    pars.Er = pars.Vr;
    pars.Vth = pars.VT;
elseif strcmpi(model_name,'EIF_Kv7')
    pars = struct( ...
    'area', 28100, ...               % [um2]
    'C', 1, ...                      % [uF/cm2] membrane capacitance
    'gL', 0.1, ...                   % [mS/cm2] leak conductance
    'EL', -55, ...                   % [mV] leak reversal potential
    'VT', -50, ...                   % [mV] spike threshold
    'deltaT', 1.4, ...               % [mV] slope factor
    'Vre', -72, ...                  % [mV] reset voltage
    'tau_ref', 2e-3, ...             % [s]  refractory period
    'gx', 0.3, ...                   % [mS/cm2]
    'taux', 0.2, ...                 % [s]
    'Vxh', -40, ...                  % [mV]
    'Dx', 8, ...                     % [mV]
    'Ex', -85);                      % [mV]
%     pars = struct( ...
%     'C', 281, ...                    % [pF] membrane capacitance
%     'gL', 30, ...                    % [nS] leak conductance
%     'EL', -70.6, ...                 % [mV] leak reversal potential
%     'VT', -50.4, ...                 % [mV] spike threshold
%     'deltaT', 2, ...                 % [mV] slope factor
%     'Vre', -55, ...                  % [mV] reset voltage
%     'tau_ref', 2e-3, ...             % [s]  refractory period
%     'gx', 10, ...                    % [nS] 0.3
%     'taux', 0.2, ...                 % [s]
%     'Vxh', -40, ...                  % [mV]
%     'Dx', 8, ...                     % [mV]
%     'Ex', -70);                      % [mV]
    pars.area = pars.area*1e-8;        % [cm2]
    pars.C = pars.C*pars.area*1e6;     % [pF]
    pars.gL = pars.gL*pars.area*1e6;   % [nS]
    pars.gx = pars.gx*pars.area*1e6;   % [nS]
    pars.Rm = 1e3/pars.gL;             % [MOhm]
    pars.Er = pars.Vre;
    pars.Vth = pars.VT;
    pars.tarp = pars.tau_ref;
else
    error('Unknown neuron model ''%s''.', model_name);
end
