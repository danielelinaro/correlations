clear all;
% close all;
clc;
addpath ~/MatlabLibrary;
addpath ~/MatlabLibrary/neuron_models;

%%
pars = make_neuron_model_pars('EIF_Kv7');

%%% Parameters of the simulated background activity
mode = 'relative';
% V_bal = linspace(-46,-36,31);
V_bal = -39:1:-19;
tau_exc = 5e-3;
tau_inh = 10e-3;
E_exc = 0;
E_inh = -80;
R_exc = 7000;
c = 0.5;
n_voltages = length(V_bal);
n_trials = 100;

%%% Parameters of the simulation
dt = 50e-6;      % [s] = 20 kHz sampling frequency
tpre = 0.5;      % [s]
tpost = 0.1;     % [s]
ttran = 0.1;     % [s]
L = 1;           % [s]
tstim = L+ttran; % [s]
tend = tstim + tpre + tpost;
t = 0:dt:tend;
V = zeros(n_trials,length(t));
idx = find(t>tpre+ttran & t<=tpre+tstim);
t = t(idx) - (tpre + ttran);

spike_times = cell(n_voltages,1);
firing_rates = cell(n_voltages,1);
y = cell(n_voltages,1);
n = cell(n_voltages,1);
bin_width = pars.tau_ref;

T = 40e-3;      % [s]

%%% Simulate the neuron
both_correlated = 1;    % if 0, only excitation is correlated, if 1 both exc. and inh. are
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));
stream = RandStream('mt19937ar','Seed',5061983);
seed_exc_c = stream.randi(10000,n_trials,1);
if both_correlated
    seed_inh_c = stream.randi(20000,n_trials,1);
end

for i=1:n_voltages
    fprintf(1, 'Voltage [%03d/%03d]:\n   ', i, n_voltages);
    ratio = computeRatesRatio(mode,V_bal(i),pars.Rm,tau_exc*1e3,tau_inh*1e3,E_exc,E_inh);
    for j=1:n_trials
        fprintf(1, '.');
        if mod(j,50) == 0
            fprintf(1, '\n');
            if j ~= n_trials
                fprintf(1, '   ');
            end
        end
        if both_correlated
            [Gm_exc_c,Gm_inh_c,Gs_exc_c,Gs_inh_c] = ...
                computeSynapticBackgroundCoefficients(mode,pars.Rm,R_exc*c,ratio,tau_exc*1e3,tau_inh*1e3);
            [Gm_exc,Gm_inh,Gs_exc,Gs_inh] = ...
                computeSynapticBackgroundCoefficients(mode,pars.Rm,R_exc*(1-c),ratio,tau_exc*1e3,tau_inh*1e3);
            Ge_c = [pre, OU_fast(tstim,dt,Gm_exc_c,Gs_exc_c,tau_exc,Gm_exc_c,seed_exc_c(j)), post];
            Gi_c = [pre, OU_fast(tstim,dt,Gm_inh_c,Gs_inh_c,tau_inh,Gm_inh_c,seed_inh_c(j)), post];
            Ge = [pre, OU_fast(tstim,dt,Gm_exc,Gs_exc,tau_exc,Gm_exc,randi(10000)), post];
            Gi = [pre, OU_fast(tstim,dt,Gm_inh,Gs_inh,tau_inh,Gm_inh,randi(10000)), post];
            V(j,:) = run_model('EIF_Kv7',tend,dt,pars,0,Ge_c,E_exc,Gi_c,E_inh,Ge,E_exc,Gi,E_inh);
        else
            [Gm_exc_c,~,Gs_exc_c] = ...
                computeSynapticBackgroundCoefficients(mode,Rm,R_exc*c,ratio,tau_exc*1e3,tau_inh*1e3);
            [Gm_exc,~,Gs_exc] = ...
                computeSynapticBackgroundCoefficients(mode,Rm,R_exc*(1-c),ratio,tau_exc*1e3,tau_inh*1e3);
            [~,Gm_inh,~,Gs_inh] = ...
                computeSynapticBackgroundCoefficients(mode,Rm,R_exc,ratio,tau_exc*1e3,tau_inh*1e3);
            Ge_c = [pre, OU_fast(tstim,dt,Gm_exc_c,Gs_exc_c,tau_exc,Gm_exc_c,seed_exc_c(j)), post];
            Ge = [pre, OU_fast(tstim,dt,Gm_exc,Gs_exc,tau_exc,Gm_exc,randi(10000)), post];
            Gi = [pre, OU_fast(tstim,dt,Gm_inh,Gs_inh,tau_inh,Gm_inh,randi(10000)), post];
            V(j,:) = run_model('EIF_Kv7',tend,dt,pars,0,Ge_c,E_exc,Ge,E_exc,Gi,E_inh);
        end
    end
    if mod(j,50)
        fprintf('\n');
    end
    spike_times{i} = extractAPPeak(t,V(:,idx),0);
    firing_rates{i} = cellfun(@(x) length(x)/L, spike_times{i});
    [y{i},~,n{i}] = binarizeSpikeTrain(spike_times{i},bin_width,[0,L],T);
end
nu = cellfun(@(x) mean(x), firing_rates);

% clear V Ge Gi Ge_c Gi_c i j t pre post ratio Gm_* Gs_* idx stream

%%
use_spike_counts = 1;

geom_mean_rate = [];
r = [];

if ~ use_spike_counts
    c_ii = zeros(n_voltages,1);
    delta = 200*bin_width;
    fprintf(1, 'Computing autocorrelations:\n   ');
    for i=1:n_voltages
        fprintf(1,'+');
        [~,~,c_ii(i)] = computeRho(y{i},y{i},L,bin_width,delta,firing_rates{i},firing_rates{i});
    end
    fprintf(1,'\n');
end
cnt = 1;
fprintf(1, 'Computing rho:\n   ');
for i=1:n_voltages
    for j=i+1:n_voltages
        fprintf(1,'.');
        if mod(cnt,100) == 0
            fprintf(1,'\n   '); 
        end
        if nu(i)/nu(j) >= 0.5 && nu(i)/nu(j) <= 2
            geom_mean_rate = [geom_mean_rate; sqrt(prod(nu([i,j])))];
            if use_spike_counts
                rho = computeCorrelationCoefficient(n(i),n(j),L,T);
            else
                rho = computeRho(y{i},y{j},L,bin_width,delta,firing_rates{i},...
                    firing_rates{j},c_ii(i),c_ii(j));
            end
            r = [r; rho];
        end
        cnt = cnt+1;
    end
end
fprintf(1,'\n');

clear i j cnt
% save(sprintf('EIF_Kv7_correlations_%s.mat',datestr(now,'yyyymmddHHMMSS')));
