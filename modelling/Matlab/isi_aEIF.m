clear all;
close all;
clc;

%%
C = 281;        % [pF] membrane capacitance
gL = 30;        % [nS] leak conductance
EL = -70.6;     % [mV] leak reversal potential
VT = -50.4;     % [mV] spike threshold
deltaT = 2;     % [mV] slope factor
tauw = 0.144;   % [s] adaptation time constant
a = 4;          % [nS] subthreshold adaptation
b = 0.0805;     % [nA] spike-triggered adaptation
Vr = [-70,-55]; % [mV] reset voltage
tarp = 0.01;    % [s] refractory period
Iext = 0;       % [pA]

tstim = 30;     % [s]
tpre = 0.1;     % [s]
tpost = 0.2;    % [s]
dt = 1/20000;   % [s]
tend = tstim+tpre+tpost;
t = 0:dt:tend;

Rm = 1e3/gL;
mode = 'relative';
V_bal = -35;
ratio = computeRatesRatio(mode,V_bal,Rm,5,10,0,-80);
[Gm_exc,Gm_inh,Gs_exc,Gs_inh] = ...
    computeSynapticBackgroundCoefficients(mode,Rm,7000,ratio,5,10);
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));
Ge = struct('E', 0, 'G', [pre, ...
    OU(tstim,dt,Gm_exc,Gs_exc,5e-3,5061983), post]);
Gi = struct('E', -80, 'G', [pre, ...
    OU(tstim,dt,Gm_inh,Gs_inh,10e-3,7051983), post]);

V = zeros(length(Vr),length(t));
w = zeros(length(Vr),length(t));
for k=1:length(Vr)
    [V(k,:),w(k,:)] = aEIF(tend,dt,C,gL,EL,deltaT,VT,tauw,a,b,Vr(k),tarp,0,Ge,Gi);
end

%%
[tp,Vp] = extractAPPeak(t,V,VT+10);
isi = cellfun(@(x) diff(x), tp, 'UniformOutput', 0);
cv = cellfun(@(x) std(x)/mean(x), isi);
f = cellfun(@(x) length(x)/tstim, tp);

%%
figure;
hold on;
plot(t,V(1,:),'k');
plot(t,V(2,:),'r');
xlabel('Time (s)');
ylabel('Membrane voltage (mV)');
set(gcf,'Color','w');
