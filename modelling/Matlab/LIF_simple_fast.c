#include "mex.h"
#include <math.h>

#define IS_SCALAR(x) (mxGetM(x) == 1 && mxGetN(x) == 1)

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
        double tend, dt, taum, *I;
        double reset=0., threshold, coeff;
        double *V;
        size_t i, n;

        /* check that the number of inputs and outputs is correct */
        if (nrhs < 4 || nrhs > 5 || nlhs > 1)
                mxErrMsgTxt("usage: V = LIF_simple_fast(tend, dt, taum, threshold, Iext)\n");

        /* simulation parameters */
        tend      = mxGetScalar(prhs[0]);
        dt        = mxGetScalar(prhs[1]);
        /* model parameters */
        taum      = mxGetScalar(prhs[2]);
        threshold = mxGetScalar(prhs[3]);
        
        /* number of steps */
        n = (size_t) round(tend/dt) + 1;

        if (nrhs == 5) {
                /* external applied current */
                if (IS_SCALAR(prhs[4])) {
                        /* the user gave just one value */
                        double tmp = mxGetScalar(prhs[4]);
                        I = mxCalloc(1, n*sizeof(double));
                        for (i=0; i<n; i++)
                                I[i] = tmp;
                }
                else {
                        /* the user gave the whole array */
                        if (mxGetM(prhs[4]) != n && mxGetN(prhs[4]) != n)
                                mxErrMsgTxt("Wrong number of elements in array Iext.\n");
                        I = mxGetData(prhs[4]);
                }
        }
        else {
                /* no external current, we set it to 0 */
                I = mxCalloc(1, n*sizeof(double));
                for (i=0; i<n; i++)
                        I[i] = 0.;
        }

        /* create the output vector and assign it to V */
        plhs[0] = mxCreateDoubleMatrix(1, n, mxREAL);
        V = mxGetData(plhs[0]);

        /* helper value */
        coeff = dt/taum;
        /* initial condition */
        V[0] = reset;
        
        /* main loop */
        for (i=1; i<n; i++) {
            V[i] = V[i-1] + coeff*(I[i-1] - V[i-1]);
            if (V[i] > threshold)
                V[++i] = reset;
        }
}

