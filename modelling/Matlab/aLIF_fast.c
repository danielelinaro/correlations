#include "mex.h"
#include <math.h>

#define SPIKE
#ifdef SPIKE
#define VPEAK 20.0
#define SPIKE_LINEAR_DECAY
#endif

#define IS_SCALAR(x) (mxGetM(x) == 1 && mxGetN(x) == 1)

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
        double tend, dt, C, gl, El, deltaT, VT, tauw, a, b, Vr, tarp, *Iext;
        double dt_over_C, dt_over_tauw, gl_times_deltaT, one_over_deltaT, Itot;
        double **G, *E;
        double *V, *w;
        size_t n, refractory_period, n_conductances, i, j;
#ifdef SPIKE_LINEAR_DECAY
        double decay_coeff;
#endif 

        /* check that the number of inputs and outputs is correct */
        if (nrhs < 11)
                mxErrMsgTxt("usage: [V,w] = aLIF_fast(tend, dt, C, gL, EL, "
                        "VT, tauw, a, b, Vr, tarp, Iext, [G_1, E_1, [G_2, E_2, ...]])");

        /* simulation parameters */
        tend   = mxGetScalar(prhs[0]) * 1e3;
        dt     = mxGetScalar(prhs[1]) * 1e3;
        /* model parameters */
        C      = mxGetScalar(prhs[2]);
        gl     = mxGetScalar(prhs[3]);
        El     = mxGetScalar(prhs[4]);
        VT     = mxGetScalar(prhs[5]);
        tauw   = mxGetScalar(prhs[6]) * 1e3;
        a      = mxGetScalar(prhs[7]);
        b      = mxGetScalar(prhs[8]);
        Vr     = mxGetScalar(prhs[9]);
        tarp   = mxGetScalar(prhs[10]) * 1e3;

        /* number of steps */
        n = (size_t) round(tend/dt) + 1;

        if (nrhs > 11) {
                /* external applied current */
                if (IS_SCALAR(prhs[11])) {
                        /* the user gave just one value */
                        double tmp = mxGetScalar(prhs[11]);
                        Iext = mxCalloc(1, n*sizeof(double));
                        for (i=0; i<n; i++)
                                Iext[i] = tmp;
                }
                else {
                        /* the user gave the whole array */
                        if (mxGetM(prhs[11]) != n && mxGetN(prhs[11]) != n)
                                mxErrMsgTxt("Wrong number of elements in array Iext.\n");
                        Iext = mxGetData(prhs[11]);
                }
                /* input conductances */
                n_conductances = (nrhs - 12)/2;
                if (n_conductances) {
                        G = (double **) mxCalloc(n_conductances,sizeof(double*));
                        E = (double *) mxCalloc(n_conductances,sizeof(double));
                        for (i=0; i<n_conductances; i++) {
                                if (IS_SCALAR(prhs[12+i*2])) {
                                        /* the user gave just one value */
                                        double tmp = mxGetScalar(prhs[12+i*2]);
                                        G[i] = mxCalloc(1, n*sizeof(double));
                                        for (j=0; j<n; j++)
                                                G[i][j] = tmp;
                                }
                                else {
                                        if (mxGetM(prhs[12+i*2]) != n && mxGetN(prhs[12+i*2]) != n)
                                                mxErrMsgTxt("Wrong number of elements in array G_%d.\n", i+1);
                                        G[i] = mxGetData(prhs[12+i*2]);
                                }
                                E[i] = mxGetScalar(prhs[13+i*2]);
                        }
                }
        }
        else {
                /* no external current, we set it to 0 */
                Iext = mxCalloc(1, n*sizeof(double));
                for (i=0; i<n; i++)
                        Iext[i] = 0.;
                /* no input conductances */
                n_conductances = 0;
        }

        /* create the output vector and assign it to V */
        plhs[0] = mxCreateDoubleMatrix(1, n, mxREAL);
        V = mxGetData(plhs[0]);
        if (nlhs > 1) {
                plhs[1] = mxCreateDoubleMatrix(1, n, mxREAL);
                w = mxGetData(plhs[1]);
        }
        else {
                w = mxCalloc(n, sizeof(double));
        }

        /* some helper values */
        dt_over_C = dt/C;
        dt_over_tauw = dt/tauw;
        gl_times_deltaT = gl*deltaT;
        one_over_deltaT = 1./deltaT;
        refractory_period = (size_t) floor(tarp/dt);

#ifdef SPIKE_LINEAR_DECAY
        decay_coeff = dt/tarp * (VPEAK-Vr);
#endif

        /* initial condition */
        V[0] = -65.;
        w[0] = 0.;

        /* main loop */
        for (i=1; i<n; i++) {
                /* compute the total current I = Iext + g_i * (E_i - V) */
                Itot = Iext[i-1];
                for (j=0; j<n_conductances; j++)
                        Itot += G[j][i-1] * (E[j] - V[i-1]);
                V[i] = V[i-1] + dt_over_C * (gl*(El-V[i-1]) + Itot - w[i-1]);
                w[i] = w[i-1] + dt_over_tauw*(a*(V[i-1]-El) - w[i-1]);
                /* is the membrane voltage above threshold? */
                if (V[i] > VT) {
#ifdef SPIKE
                        V[i] = VPEAK;
#endif
                        w[i] += b;
#ifndef SPIKE_LINEAR_DECAY
                        /*
                         * clamp the membrane voltage to Vr for the duration
                         * of the refractory period and increase the adaptation
                         * variable
                         */
                        if (refractory_period) {
                            for (j=0; j<refractory_period; j++) {
                                    V[++i] = Vr;
                                    w[i] = w[i-j-1];
                            }
                        }
                        else {
                            V[++i] = Vr;
                            w[i] = w[i-1] + dt_over_tauw*(a*(V[i-1]-El) - w[i-1]);
                        }
#else
                        /* linear decay of the membrane voltage to the
                         * reset voltage */
                        if (refractory_period) {
                            for (j=0; j<refractory_period; j++) {
                                    if (i == n-1)
                                            return;
                                    V[++i] = V[i-1] - decay_coeff;
                                    w[i] = w[i-j-1];
                            }
                        }
                        else {
                            V[++i] = Vr;
                            w[i] = w[i-1] + dt_over_tauw*(a*(V[i-1]-El) - w[i-1]);
                        }
#endif
                }
        }
}

