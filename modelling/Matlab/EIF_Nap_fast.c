#include "mex.h"
#include <math.h>

#define IS_SCALAR(x) (mxGetM(x) == 1 && mxGetN(x) == 1)

#define ENA 55.
#define VTH 30.

double minf(double V) {
    return 1 / (1 + exp(-0.1*(V+48)));
}

double taum(double V) {
    if (V < -40.)
        return 0.025+0.14*exp(0.1*(V+40.));
    return 0.02+0.145*exp(-0.1*(V+40.));
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
        double tend, dt, C, gnap, gl, El, deltaT, VT, Vr, tarp, *Iext;
        double dt_over_C, gl_times_deltaT, one_over_deltaT, decay_coeff, Itot;
        double **G, *E;
        double *V, *m;
        size_t n, refractory_period, n_conductances, i, j;

        /* check that the number of inputs and outputs is correct */
        if (nrhs < 10 || nlhs < 1)
                mxErrMsgTxt("usage: [V,m] = EIF_Nap_fast(tend, dt, C, gNap, gL, EL, deltaT, "
                        "VT, Vr, tarp, Iext, [G_1, E_1, [G_2, E_2, ...]])\n");

        /* simulation parameters */
        tend   = mxGetScalar(prhs[0]) * 1e3;
        dt     = mxGetScalar(prhs[1]) * 1e3;
        /* model parameters */
        C      = mxGetScalar(prhs[2]);
        gnap   = mxGetScalar(prhs[3]);
        gl     = mxGetScalar(prhs[4]);
        El     = mxGetScalar(prhs[5]);
        deltaT = mxGetScalar(prhs[6]);
        VT     = mxGetScalar(prhs[7]);
        Vr     = mxGetScalar(prhs[8]);
        tarp   = mxGetScalar(prhs[9]) * 1e3;

        /* number of steps */
        n = (size_t) round(tend/dt) + 1;

        if (nrhs > 10) {
                /* external applied current */
                if (IS_SCALAR(prhs[10])) {
                        /* the user gave just one value */
                        double tmp = mxGetScalar(prhs[10]);
                        Iext = mxCalloc(1, n*sizeof(double));
                        for (i=0; i<n; i++)
                                Iext[i] = tmp;
                }
                else {
                        /* the user gave the whole array */
                        if (mxGetM(prhs[10]) != n && mxGetN(prhs[10]) != n)
                                mxErrMsgTxt("Wrong number of elements in array Iext.\n");
                        Iext = mxGetData(prhs[10]);
                }
                /* input conductances */
                n_conductances = (nrhs - 11)/2;
                if (n_conductances) {
                        G = (double **) mxCalloc(n_conductances,sizeof(double*));
                        E = (double *) mxCalloc(n_conductances,sizeof(double));
                        for (i=0; i<n_conductances; i++) {
                                if (IS_SCALAR(prhs[11+i*2])) {
                                        /* the user gave just one value */
                                        double tmp = mxGetScalar(prhs[11+i*2]);
                                        G[i] = mxCalloc(1, n*sizeof(double));
                                        for (j=0; j<n; j++)
                                                G[i][j] = tmp;
                                }
                                else {
                                        if (mxGetM(prhs[11+i*2]) != n && mxGetN(prhs[11+i*2]) != n)
                                                mxErrMsgTxt("Wrong number of elements in array G_%d.\n", i+1);
                                        G[i] = mxGetData(prhs[11+i*2]);
                                }
                                E[i] = mxGetScalar(prhs[12+i*2]);
                        }
                }
        }
        else {
                /* no external current, we set it to 0 */
                Iext = mxCalloc(1, n*sizeof(double));
                for (i=0; i<n; i++)
                        Iext[i] = 0.;
                /* no input conductances */
                n_conductances = 0;
        }

        /* create the output vector and assign it to V */
        plhs[0] = mxCreateDoubleMatrix(1, n, mxREAL);
        V = mxGetData(plhs[0]);
        if (nlhs > 1) {
                plhs[1] = mxCreateDoubleMatrix(1, n, mxREAL);
                m = mxGetData(plhs[1]);
        }
        else {
                m = mxCalloc(n, sizeof(double));
        }

        /* some helper values */
        dt_over_C = dt/C;
        gl_times_deltaT = gl*deltaT;
        one_over_deltaT = 1./deltaT;
        refractory_period = (size_t) floor(tarp/dt);
        decay_coeff = dt/tarp * (VTH-Vr);

        /* initial condition */
        V[0] = -65.;
        m[0] = 0.;

        /* main loop */
        for (i=1; i<n; i++) {
                /* compute the total current I = Iext + g_i * (E_i - V) */
                Itot = Iext[i-1];
                for (j=0; j<n_conductances; j++)
                        Itot += G[j][i-1] * (E[j] - V[i-1]);
                V[i] = V[i-1] + dt_over_C * (gl*(El-V[i-1]) + gnap*m[i-1]*(ENA-V[i-1]) + 
                        gl_times_deltaT*exp(one_over_deltaT*(V[i-1]-VT)) + Itot);
                m[i] = m[i-1] + dt/taum(V[i-1])*(minf(V[i-1]) - m[i-1]);
                /* is the membrane voltage above threshold? */
                if (V[i] > VTH) {
                        V[i] = VTH;  
                        /* linear decay of the membrane voltage to the
                         * reset voltage */
                        for (j=0, i++; j<refractory_period; j++, i++) {
                                if (i == n)
                                        return;
                                V[i] = V[i-1] - decay_coeff;
                                m[i] = m[i-1] + dt/taum(V[i-1])*(minf(V[i-1]) - m[i-1]);
                        }
                        i--;
                }
        }
}

