clear all;
close all;
clc;
addpath ~/MatlabLibrary/neuron_models/

%%
C = 1;          % [uF/cm^2] membrane capacitance
gL = 0.1;       % [mS/cm^2] leak conductance
EL = -65;       % [mV] leak reversal potential
VT = -50;       % [mV] spike threshold
deltaT = 1.4;   % [mV] slope factor
Vre = -72;      % [mV] reset voltage
tau_ref = 2;    % [ms]  refractory period
Iext = 0;       % [pA]
gx = 0.3;       % [mS/cm^2]
taux = 200;     % [ms]
Vxh = -40;      % [mV]
Dx = 8;         % [mV]
Ex = -70;       % [mV]

tstim = 1000;   % [ms]
tpre = 100;     % [ms]
tpost = 200;    % [ms]
dt = 1/20;      % [ms]
tend = tstim+tpre+tpost;
t = 0:dt:tend;
I = OU_fast(tend,dt,3,0,20,0.5);
% I = 0.5 + zeros(size(t));
I(t<tpre | t>tpre+tstim) = 0;

V = zeros(2,length(t));
x = zeros(2,length(t));
[V(1,:),x(1,:)] = EIF_Kv7_fast(tend,dt,C,gL,EL,deltaT,VT,Vre,tau_ref,0,Vxh,Dx,taux,Ex,I);
[V(2,:),x(2,:)] = EIF_Kv7_fast(tend,dt,C,gL,EL,deltaT,VT,Vre,tau_ref,gx,Vxh,Dx,taux,Ex,I);
% [tp,Vp] = extractAPPeak(t*1e-3,V,0);
figure;
subplot(2,1,1)
hold on;
plot(t,V(1,:),'k');
plot(t,V(2,:),'r');
subplot(2,1,2)
hold on;
plot(t,x(1,:),'k');
plot(t,x(2,:),'r--');

%%
orange = [1,0.5,0];
figure;
axes('Position',[0.1,0.925,0.8,0.05],'NextPlot','Add');
plot(t,I,'k');
axis([tpre-0.1,tstim+tpre+0.1,1.2*min(I),1.2*max(I)]);
axis off;

axes('Position',[0.1,0.6,0.8,0.3],'NextPlot','Add');
plot(t,V,'k');
plot(tp{1},Vp{1},'o','Color',orange,'MarkerFaceColor',orange,'MarkerSize',3);
plot(t([1,end]),VT+[0,0],'r--');
axis([tpre-0.1,tstim+tpre+0.1,-80,30]);
ylabel('V (mV)');

axes('Position',[0.1,0.4,0.8,0.1],'NextPlot','Add');
plot(t,x,'k');
axis([tpre-0.1,tstim+tpre+0.1,0,1.2*max(x)]);
xlabel('Time (s)');
ylabel('w (pA)');

axes('Position',[0.1,0.1,0.8,0.2],'NextPlot','Add');
plot(1./diff(tp{1}),'o','Color',orange,'MarkerFaceColor',orange,'MarkerSize',3);
xlabel('Spike #');
ylabel('Instantaneous frequency');

set(gcf,'Color','w');
