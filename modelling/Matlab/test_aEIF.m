clear all;
close all;
clc;

%%
C = 281;        % [pF] membrane capacitance
gL = 30;        % [nS] leak conductance
EL = -70.6;     % [mV] leak reversal potential
VT = -50.4;     % [mV] spike threshold
deltaT = 2;     % [mV] slope factor
tauw = 0.144;   % [s]  adaptation time constant
a = 4;          % [nS] subthreshold adaptation
b = 0.0805;     % [nA] spike-triggered adaptation
Vr = -55;       % [mV] reset voltage
tarp = 4e-3;    % [s]  refractory period
Iext = 900;     % [pA]

tstim = 1;      % [s]
tpre = 0.1;     % [s]
tpost = 0.2;    % [s]
dt = 1e-5;      % [s]
tend = tstim+tpre+tpost;
t = 0:dt:tend;
I = zeros(size(t));
I(t>tpre & t<tpre+tstim) = Iext;

[V,w] = aEIF_fast(tend,dt,C,gL,EL,deltaT,VT,tauw,a,b,Vr,tarp,I);
[tp,Vp] = extractAPPeak(t,V,VT);
[tth,Vth] = extractAPThreshold(t,V,VT);

%%
orange = [1,0.5,0];
figure;
axes('Position',[0.1,0.925,0.8,0.05],'NextPlot','Add');
plot(t,I,'k');
axis([tpre-0.1,tstim+tpre+0.1,0,1.2*max(I)]);
axis off;

axes('Position',[0.1,0.6,0.8,0.3],'NextPlot','Add');
plot(t,V,'k');
plot(tp{1},Vp{1},'o','Color',orange,'MarkerFaceColor',orange,'MarkerSize',3);
plot(tth{1},Vth{1},'o','Color',[1,0,0],'MarkerFaceColor','r','MarkerSize',3);
plot(t([1,end]),VT+[0,0],'r--');
axis([tpre-0.1,tstim+tpre+0.1,-80,30]);
ylabel('V (mV)');

axes('Position',[0.1,0.4,0.8,0.1],'NextPlot','Add');
plot(t,w,'k');
axis([tpre-0.1,tstim+tpre+0.1,-10,1.2*max(w)]);
xlabel('Time (s)');
ylabel('w (pA)');

axes('Position',[0.1,0.1,0.8,0.2],'NextPlot','Add');
plot(1./diff(tp{1}),'o','Color',orange,'MarkerFaceColor',orange,'MarkerSize',3);
xlabel('Spike #');
ylabel('Instantaneous frequency');

set(gcf,'Color','w');
