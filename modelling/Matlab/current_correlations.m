clear all;
close all;
clc;

%%
% Mono- and bi-exponential functions
exp_func = @(param,x) param(1)*exp(-x/param(2));
exp2_func = @(param,x) param(1)*exp(-x/param(2)) + param(3)*exp(-x/param(4));

% Model parameters
C = 0.0003;       % [?F]
gl = 0.03;        % [uS]
El = -65;         % [mV]
Er = -55;         % [mV]
tarp = 4e-3;      % [ms]
V0 = -45;         % [mV]
deltaV = 1;       % [mV]
Rm = 1/gl;        % [MOhm]

% Spike-triggered current parameters
eta1 = 0.05;      % [nA]
eta_tau1 = 0.2;   % [s]
eta2 = 0.1;       % [nA]
eta_tau2 = 0.02;  % [s]

% Moving threshold parameters
% double exponential model
% gamma1 = 0.5;       % [mV]
% gamma_tau1 = 0.15;  % [s]
% gamma2 = 1;         % [mV]
% gamma_tau2 = 0.045; % [s]
% single exponential model
gamma1 = 5;          % [mV]
gamma_tau1 = 0.02;   % [s]

% Parameters of the simulated background activity
mode = 'relative';
V_bal = [-50,-45,-40,-30];
tau_exc = 5e-3;
tau_inh = 10e-3;
E_exc = 0;
E_inh = -80;
R_exc = 7000;
n_voltages = length(V_bal);

c = 0.5;

%%% Parameters of the simulation
n_trials = 2;
dt = 50e-6;     % [s] = 20 kHz sampling frequency
tpre = 0.2;     % [s]
tpost = 0.2;    % [s]
ttran = 0.2;    % [s]
L = 100;        % [s]
tstim = L+ttran;
tend = tstim + tpre + tpost;
t = 0:dt:tend;
eta = exp2_func([eta1, eta_tau1, eta2, eta_tau2], t);
% gamma = exp2_func([gamma1, gamma_tau1, gamma2, gamma_tau2], t);
gamma = exp_func([gamma1, gamma_tau1], t);
V = zeros(n_trials,length(t));
I = zeros(n_trials,length(t));
idx = find(t>tpre+ttran & t<=tpre+tstim);
T = (t(idx) - (tpre + ttran));

window = [0:0.2e-3:tarp , tarp+0.5e-3:1e-3:10.5e-3];
Icorr = zeros(n_voltages,length(window));
    
spike_times = cell(n_voltages,1);
firing_rates = cell(n_voltages,1);

threshold = 0;

%%% Simulate the neuron
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));
seed_exc_c = 5061983;
seed_inh_c = 70583;

for i=1:n_voltages
    fprintf(1, 'Voltage [%03d/%03d]:\n   ', i, n_voltages);
    ratio = computeRatesRatio(mode,V_bal(i),Rm,tau_exc*1e3,tau_inh*1e3,E_exc,E_inh);
    for j=1:n_trials
        fprintf(1, '.');
        if mod(j,50) == 0
            fprintf(1, '\n');
            if j ~= n_trials
                fprintf(1, '   ');
            end
        end
        [Gm_exc_c,Gm_inh_c,Gs_exc_c,Gs_inh_c] = ...
            computeSynapticBackgroundCoefficients(mode,Rm,R_exc*c,ratio,tau_exc*1e3,tau_inh*1e3);
        [Gm_exc,Gm_inh,Gs_exc,Gs_inh] = ...
            computeSynapticBackgroundCoefficients(mode,Rm,R_exc*(1-c),ratio,tau_exc*1e3,tau_inh*1e3);
        Gm_exc_c = Gm_exc_c*1e-3;
        Gm_inh_c = Gm_inh_c*1e-3;
        Gs_exc_c = Gs_exc_c*1e-3;
        Gs_inh_c = Gs_inh_c*1e-3;
        Gm_exc = Gm_exc*1e-3;
        Gm_inh = Gm_inh*1e-3;
        Gs_exc = Gs_exc*1e-3;
        Gs_inh = Gs_inh*1e-3;
        Ge_c = [pre, OU_fast(tstim,dt,Gm_exc_c,Gs_exc_c,tau_exc,Gm_exc_c,seed_exc_c), post];
        Gi_c = [pre, OU_fast(tstim,dt,Gm_inh_c,Gs_inh_c,tau_inh,Gm_inh_c,seed_inh_c), post];
        Ge = [pre, OU_fast(tstim,dt,Gm_exc,Gs_exc,tau_exc,Gm_exc,randi(10000)), post];
        Gi = [pre, OU_fast(tstim,dt,Gm_inh,Gs_inh,tau_inh,Gm_inh,randi(10000)), post];
        V(j,:) = IF_eta_fast(tend,dt,C,gl,El,Er,tarp,V0,deltaV,eta,gamma,0,Ge_c,...
            E_exc,Gi_c,E_inh,Ge,E_exc,Gi,E_inh);
        I(j,:) = (Ge+Ge_c) .* (E_exc-V(j,:)) + (Gi+Gi_c) .* (E_inh-V(j,:));
    end
    if mod(j,50)
        fprintf('\n   ');
    end
    spike_times{i} = extractAPPeak(T,V(:,idx),threshold);
    firing_rates{i} = cellfun(@(x) length(x)/L, spike_times{i});

    Vorig = V(:,idx);
    Iorig = I(:,idx);
    for j=1:length(window)
        fprintf(1, '.');
        Vtrimmed = trimSpikes(T,Vorig,{'threshold','threshold'},[0e-3,window(j)],-20,'nan');
        jdx = find(~isnan(Vtrimmed(1,:)) & ~isnan(Vtrimmed(2,:)));
        tmp = corr(Iorig(:,jdx)');
        Icorr(i,j) = tmp(2);
    end
    fprintf(1, '\n');
  
end
nu = cellfun(@(x) mean(x), firing_rates);
clear Ge Gi Ge_c Gi_c i j pre post ratio Gm_* Gs_* tmp

%%
hndl = zeros(n_voltages,1);
lgnd = cell(n_voltages,1);
col = [1,.5,0 ; 0,1,.5 ; 1,0,0.5 ; 0,.5,1];
figure;
axes('Position',[0.1,0.1,0.5,0.85],'NextPlot','Add');
plot([0,10],[c,c],'--','Color',[.3,.3,.3],'LineWidth',2);
plot(tarp*[1e3,1e3],[0,c+0.05],'Color',[.75,.75,.75],'LineWidth',2);
for i=1:n_voltages
    hndl(i) = plot(window*1e3,Icorr(i,:),'s-','Color',col(i,:),...
        'MarkerFaceColor',col(i,:),'LineWidth',1);
    lgnd{i} = sprintf('nu = %g Hz', nu(i));
end
text(10,c+0.03,sprintf('c = %g',c),'HorizontalAlignment','Right',...
    'VerticalAlignment','Top');
text(tarp*1e3+0.1,c+0.03,sprintf('tau_{arp} = %g ms',tarp*1e3),...
    'HorizontalAlignment','Left','VerticalAlignment','Top');
xlabel('\Delta t (ms)');
ylabel('Current correlation');
h = legend(hndl,lgnd,'Location','SouthEast');
set(h,'Box','Off');
box off;
set(gca,'TickDir','out','XTick',0:2:10,'YTick',0:.1:.5);
axis([0,10,0,0.55]);

axes('Position',[0.65,0.1,0.32,0.85],'NextPlot','Add');
spk = find(diff(spike_times{end}{1}) > 80e-3,1);
idx = find(T > spike_times{end}{1}(spk)-20e-3 & T < spike_times{end}{1}(spk)+60e-3);
plot(T(idx),Vorig(1,idx),'k');
placeScaleBars(T(idx(1)),-50,[],5,'','5 mV','k');
placeScaleBars(T(idx(1)),min(Vorig(1,idx)),20e-3,[],'20 ms','','k');
axis([T(idx([1,end]))+[-1e-3,1e-3],min(Vorig(1,idx)),-40]);
axis off;

axes('Position',[0.8,0.6,0.15,0.35],'NextPlot','Add');
idx = find(T > spike_times{end}{1}(spk)-2e-3 & T < spike_times{end}{1}(spk)+5e-3);
plot(T(idx),Vorig(1,idx),'k');
placeScaleBars(T(idx(end)),-50,[],5,'','5 mV','k');
placeScaleBars(spike_times{end}{1}(spk),min(Vorig(1,idx))-2,tarp,[],...
    sprintf('%g ms',tarp*1e3),'','k');
axis([T(idx([1,end])),min(Vorig(1,idx))-3,-40]);
axis off;

eps_filename = sprintf('current_correlations_IFeta_tarp_%d_ms_L_%d_s.eps', tarp*1e3, L);
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,9,6]);
print('-depsc2',eps_filename);

