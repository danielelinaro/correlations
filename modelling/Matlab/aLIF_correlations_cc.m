clear all;
close all;
clc;
addpath ~/MatlabLibrary;
addpath ~/MatlabLibrary/neuron_models;

%%
C = 281;        % [pF]   membrane capacitance
gL = 20;        % [nS]   leak conductance
EL = -70.6;     % [mV]   leak reversal potential
VT = -50.4;     % [mV]   spike threshold
tauw = 0.01;    % [s]    adaptation time constant
a = 0;          % [nS]   subthreshold adaptation
b = 500;        % [pA]   spike-triggered adaptation
Vr = -55;       % [mV]   reset voltage
tarp = 2e-3;    % [s]    refractory period
Iext = 0;       % [pA]   externally applied current
Rm = 1e3/gL;    % [MOhm] input resistance

if b == 0
    Im = logspace(log10(230),log10(400),30);
    Is = [120,150];
elseif b == 500
    Im = logspace(log10(300),log10(500),20);
    Is = [100,200];
elseif b == 200
    Im = logspace(log10(300),log10(500),20);
    Is = [100,200];
end
Itau = 2e-3;
c = 0.5;

tmp = repmat(Is(:)',[length(Im),1]);
pars = [repmat(Im(:),[length(Is),1]), tmp(:)];
n_pars = size(pars,1);

%%% Parameters of the simulation
n_trials = 50;
dt = 50e-6;     % [s] = 20 kHz sampling frequency
tpre = 0.5;     % [s]
tpost = 0.1;    % [s]
ttran = 1;      % [s]
L = 100;        % [s]
tstim = L+ttran;      % [s]
tend = tstim + tpre + tpost;
t = 0:dt:tend;
V = zeros(n_trials,length(t));
idx = find(t>tpre+ttran & t<=tpre+tstim);
t = t(idx) - (tpre + ttran);

spike_times = cell(n_pars,1);
firing_rates = cell(n_pars,1);
y = cell(n_pars,1);
bin_width = tarp;

%%% Simulate the neuron
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));
stream = RandStream('mt19937ar','Seed',5061983);
seeds = stream.randi(10000,n_trials,1);

for i=1:n_pars
    fprintf(1, '[%03d/%03d] Im = %f, Is = %f:\n   ', i, n_pars, pars(i,1), pars(i,2));
    for j=1:n_trials
        fprintf(1, '.');
        if mod(j,50) == 0
            fprintf(1, '\n');
            if j ~= n_trials
                fprintf(1, '   ');
            end
        end
        I = [pre, pars(i,1) + pars(i,2) * ...
            (sqrt(1-c) * OU_fast(tstim,dt,0,1,Itau,0,randi(10000)) + ...
             sqrt(c) * OU_fast(tstim,dt,0,1,Itau,0,seeds(j))), post];
        V(j,:) = aLIF_fast(tend,dt,C,gL,EL,VT,tauw,a,b,Vr,tarp,I);
    end
    if mod(j,50)
        fprintf('\n');
    end
    spike_times{i} = extractAPPeak(t,V(:,idx),0);
    firing_rates{i} = cellfun(@(x) length(x)/L, spike_times{i});
    y{i} = binarizeSpikeTrain(spike_times{i},bin_width,[0,L]);
end
nu = cellfun(@(x) mean(x), firing_rates);

clear V Ge Gi Ge_c Gi_c i j t pre post ratio Gm_* Gs_* idx stream I tmp

%%
geom_mean_rate = [];
r = [];
c_ii = zeros(n_pars,1);
delta = 140*bin_width;
fprintf(1, 'Computing autocorrelations:\n   ');
for i=1:n_pars
    fprintf(1,'+');
    [~,~,c_ii(i)] = computeRho(y{i},y{i},L,bin_width,delta,firing_rates{i},firing_rates{i});
end
fprintf(1,'\n');
cnt = 1;
fprintf(1, 'Computing rho:\n   ');
for i=1:n_pars
    for j=i+1:n_pars
        fprintf(1,'.');
        if mod(cnt,100) == 0
            fprintf(1,'\n   '); 
        end
        if nu(i)/nu(j) >= 0.5 && nu(i)/nu(j) <= 2
            geom_mean_rate = [geom_mean_rate; sqrt(prod(nu([i,j])))];
            r = [r; computeRho(y{i},y{j},L,bin_width,delta,firing_rates{i},...
                firing_rates{j},c_ii(i),c_ii(j))];
        end
        cnt = cnt+1;
    end
end
fprintf(1,'\n');

clear i j cnt
save(sprintf('aLIF_correlations_cc_%s.mat',datestr(now,'yyyymmddHHMMSS')));
