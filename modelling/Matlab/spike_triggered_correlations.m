clear all;
close all;
clc;
addpath ~/MatlabLibrary;
addpath ~/MatlabLibrary/neuron_models;

%%
neuron_type = 'EIF_Kv7';
pars = make_neuron_model_pars(neuron_type);

%%% Parameters of the simulated background activity
mode = 'relative';
% V_bal = linspace(-50,-40,31);
%%% LIF
% V_bal = [-48,-47.5];   % 0.5 Hz
% V_bal = [-43.7,-43.6]; % 5 Hz
% V_bal = [-42,-41.9];   % 10 Hz
% V_bal = [-39.6,-39.4]; % 20 Hz
%%% aEIF
% V_bal = [-43.7,-43.6]; % 0.5 Hz
% V_bal = [-38.1,-37.9]; % 5 Hz
% V_bal = [-35.5,-35.4]; % 10 Hz
% V_bal = [-32.6,-32.4]; % 20 Hz
%%% EIF_Kv7
% V_bal = [-37.5,-36.5]; % 0.5 Hz
% V_bal = [-29.6,-29.5]; % 5 Hz
% V_bal = [-26,-25.5]; % 10 Hz
V_bal = [-20.6,-20.8]; % 20 Hz
tau_exc = 5e-3;
tau_inh = 10e-3;
E_exc = 0;
E_inh = -80;
R_exc = 7000;
c = 0.5;
n_voltages = length(V_bal);
n_trials = 50;

%%% Parameters of the simulation
dt = 50e-6;     % [s] = 20 kHz sampling frequency
tpre = 0.5;     % [s]
tpost = 0.1;    % [s]
ttran = 1;      % [s]
L = 100;        % [s]
tstim = L+ttran;      % [s]
tend = tstim + tpre + tpost;
t = 0:dt:tend;
V = cell(n_voltages,1);
I = cell(n_voltages,1);
idx = find(t>tpre+ttran & t<=tpre+tstim);
t = t(idx) - (tpre + ttran);

firing_rates = cell(n_voltages,1);
bin_width = pars.tarp;

Vp = 20;
if strcmpi(neuron_type,'LIF')
    nafter = round((Vp-pars.Vth)/((Vp-pars.Er)/(pars.tarp/dt)));
    nbefore = 1;
elseif strcmpi(neuron_type,'aEIF')
    nafter = round((Vp-pars.VT)/((Vp-pars.Vr)/(pars.tarp/dt)));
    nbefore = 5;
elseif strcmpi(neuron_type,'EIF_Kv7')
    nafter = round((Vp-pars.VT)/((Vp-pars.Vre)/(pars.tau_ref/dt)));
    nbefore = 5;
end

%%% Simulate the neuron
both_correlated = 1;    % if 0, only excitation is correlated, if 1 both exc. and inh. are
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));
stream = RandStream('mt19937ar','Seed',5061983);
seed_exc_c = stream.randi(10000,n_trials,1);
if both_correlated
    seed_inh_c = stream.randi(20000,n_trials,1);
end

for i=1:n_voltages
    fprintf(1, 'Voltage [%03d/%03d]:\n   ', i, n_voltages);
    V{i} = zeros(n_trials,length(t));
    I{i} = zeros(n_trials,length(t));
    ratio = computeRatesRatio(mode,V_bal(i),pars.Rm,tau_exc*1e3,tau_inh*1e3,E_exc,E_inh);
    for j=1:n_trials
        fprintf(1, '.');
        if mod(j,50) == 0
            fprintf(1, '\n');
            if j ~= n_trials
                fprintf(1, '   ');
            end
        end
        if both_correlated
            [Gm_exc_c,Gm_inh_c,Gs_exc_c,Gs_inh_c] = ...
                computeSynapticBackgroundCoefficients(mode,pars.Rm,R_exc*c,ratio,tau_exc*1e3,tau_inh*1e3);
            [Gm_exc,Gm_inh,Gs_exc,Gs_inh] = ...
                computeSynapticBackgroundCoefficients(mode,pars.Rm,R_exc*(1-c),ratio,tau_exc*1e3,tau_inh*1e3);
            Ge_c = [pre, OU_fast(tstim,dt,Gm_exc_c,Gs_exc_c,tau_exc,Gm_exc_c,seed_exc_c(j)), post];
            Gi_c = [pre, OU_fast(tstim,dt,Gm_inh_c,Gs_inh_c,tau_inh,Gm_inh_c,seed_inh_c(j)), post];
            Ge = [pre, OU_fast(tstim,dt,Gm_exc,Gs_exc,tau_exc,Gm_exc,randi(10000)), post];
            Gi = [pre, OU_fast(tstim,dt,Gm_inh,Gs_inh,tau_inh,Gm_inh,randi(10000)), post];
            voltage = run_model(neuron_type,tend,dt,pars,0,Ge_c,E_exc,Ge,E_exc,Gi_c,E_inh,Gi,E_inh);
            %voltage = run_model(neuron_type,tend,dt,pars,2.75);
            current = (Ge+Ge_c).*(E_exc - voltage) + (Gi+Gi_c).*(E_inh - voltage);
            V{i}(j,:) = voltage(idx);
            I{i}(j,:) = current(idx);
        else
            [Gm_exc_c,~,Gs_exc_c] = ...
                computeSynapticBackgroundCoefficients(mode,pars.Rm,R_exc*c,ratio,tau_exc*1e3,tau_inh*1e3);
            [Gm_exc,~,Gs_exc] = ...
                computeSynapticBackgroundCoefficients(mode,pars.Rm,R_exc*(1-c),ratio,tau_exc*1e3,tau_inh*1e3);
            [~,Gm_inh,~,Gs_inh] = ...
                computeSynapticBackgroundCoefficients(mode,pars.Rm,R_exc,ratio,tau_exc*1e3,tau_inh*1e3);
            Ge_c = [pre, OU_fast(tstim,dt,Gm_exc_c,Gs_exc_c,tau_exc,Gm_exc_c,seed_exc_c(j)), post];
            Ge = [pre, OU_fast(tstim,dt,Gm_exc,Gs_exc,tau_exc,Gm_exc,randi(10000)), post];
            Gi = [pre, OU_fast(tstim,dt,Gm_inh,Gs_inh,tau_inh,Gm_inh,randi(10000)), post];
            voltage = run_model(neuron_type,tend,dt,pars,0,Ge_c,E_exc,Ge,E_exc,Gi,E_inh);
            current = (Ge+Ge_c).*(E_exc - voltage) + Gi.*(E_inh - voltage);
            V{i}(j,:) = voltage(idx);
            I{i}(j,:) = current(idx);
        end
    end
    if mod(j,50)
        fprintf('\n');
    end
    features(i).tp = extractAPPeak(t,V{i},0);
    features(i).tth = cellfun(@(x) x-nbefore*dt, features(i).tp, 'UniformOutput', 0);
    features(i).tend = cellfun(@(x) x+nafter*dt, features(i).tp, 'UniformOutput', 0);
    firing_rates{i} = cellfun(@(x) length(x)/L, features(i).tp);
end
nu = cellfun(@(x) mean(x), firing_rates);

%%
t0 = 0;
ttran = 0;
window = [20e-3,50e-3];
[stc_V,stc_I] = computeSpikeTriggeredCorrelation(t,V,I,features,t0,ttran,L,window,1);
clear t V I idx i j pre post stream ratio Gm* Gs* Ge* Gi* voltage current
filename = sprintf('spike_triggered_correlations_%s_nu_%.3f',neuron_type,nu(2));
save([filename,'.mat'])
print('-dpdf',[filename,'.pdf']);

%%
model_names = {'LIF','aEIF','EIF_Kv7'};
n = length(model_names);
for i=1:n
    files = dir(sprintf('spike_triggered_correlations_%s_nu_*.mat', model_names{i}));
    stc_I = [];
    nu = [];
    T = [];
    for j=1:length(files)
        data = load(files(j).name);
        T = [T ; -data.window(1):data.dt:data.window(2)];
        stc_I = [stc_I ; mean(data.stc_I)];
        nu = [nu ; sqrt(prod(data.nu))];
    end
    T = T*1e3;
    [nu,idx] = sort(nu);
    stc_I = stc_I(idx,:);
    subplot(1,n,i);
    hold on;
    cmap = 'krbm';
    hndl = zeros(1,length(files));
    lgnd = cell(1,length(files));
    for j=1:length(files)
        hndl(j) = plot(T(j,:),stc_I(j,:),cmap(j));
        lgnd{j} = sprintf('f = %.1f spikes/sec', nu(j));
    end
    hndl = legend(hndl,lgnd,'Location','SouthEast');
    set(hndl, 'Box', 'Off');
%     cla;
%     plot(T(1,:),mean(stc_I),'k');
    axis([T([1,end]),0,0.55]);
    set(gca,'TickDir','Out','XTick',min(T,[],2):20:max(T,[],2));
    if i == 1
        ylabel('Current correlation');
    end
    xlabel('Time since spike (ms)');
    title(strrep(model_names{i},'_',' '));
end
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,12,5],'PaperSize',[12,5]);
print('-depsc2','spike_triggered_correlations.eps');

