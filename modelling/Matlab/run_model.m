function varargout = run_model(model_name, tend, dt, pars, varargin)

if strcmpi(model_name,'LIF')
    nargoutchk(1,1);
    V = LIF_fast(tend,dt,pars.C,pars.tau,pars.tarp,pars.Er,pars.E0,pars.Vth,...
        varargin{:});
    varargout{1} = V;
elseif strcmpi(model_name,'aEIF')
    nargoutchk(1,2);
    [V,w] = aEIF_fast(tend,dt,pars.C,pars.gL,pars.EL,pars.deltaT,pars.VT,...
        pars.tauw,pars.a,pars.b,pars.Vr,pars.tarp,varargin{:});
    varargout{1} = V;
    if nargout == 2
        varargout{2} = w;
    end
elseif strcmpi(model_name,'EIF_Kv7')
    nargoutchk(1,2);
    args = cellfun(@(x) x, varargin, 'UniformOutput', 0);
    [V,x] = EIF_Kv7_fast(tend,dt,pars.C,pars.gL,pars.EL,pars.deltaT,pars.VT,...
        pars.Vre,pars.tau_ref,pars.gx,pars.Vxh,pars.Dx,pars.taux,pars.Ex,args{:});
    varargout{1} = V;
    if nargout == 2
        varargout{2} = x;
    end
else
    error('Unknown neuron model ''%s''.', model_name);
end

