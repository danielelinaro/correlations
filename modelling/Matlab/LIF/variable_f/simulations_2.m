clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary
addpath ..

%%
%%% time constant of the cells
tau = 6.5e-3:1e-3:10.5e-3;    % [s]
%%% balanced voltages
V_bal = -46:-35;
% number of trials
ntrials = 100;
% correlation coefficient
c = 0.5;
ID = 5+(1:length(tau));

for i=1:length(ID)
    for j=1:length(V_bal)
        fprintf('ID = %d - V_bal = %.2f.\n', ID(i), V_bal(j));
        simulateLIF(ID(i), tau(i), V_bal(j), c, ntrials);
    end
end

