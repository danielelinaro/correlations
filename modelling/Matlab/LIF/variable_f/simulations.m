clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary
addpath ..

%%
%%% time constant of the cells
tau = 6e-3:1e-3:10e-3;    % [s]
%%% balanced voltages
V_bal = -46:2:-36;
% number of trials
ntrials = 100;
% correlation coefficient
c = 0.5;
ID = 1:length(tau);

for id=ID
    for V=V_bal
        fprintf('ID = %d - V_bal = %.2f.\n', id, V);
        simulateLIF(id, tau(id), V, c, ntrials);
    end
end

%%
% T = 40e-3;
% files = dir('*.mat');
% for k=1:length(files)
%     load(files(k).name);
%     L = tstim - ttran;
%     [y,edges,n] = binarizeSpikeTrain(spiketimes,binWidth,[0,L],T);
%     save(files(k).name, 'C','E0','E_exc','E_inh','Er','Gm_exc','Gm_exc_c',...
%         'Gm_inh','Gm_inh_c','Gs_exc','Gs_exc_c','Gs_inh','Gs_inh_c','ID','L',...
%         'R_exc','Rm','T','V','V_bal','Vth','binWidth','c','cv','dt','edges',...
%         'isi','mode','n','ntrials','nu','rates','ratio','seed_exc_c','seed_inh_c',...
%         'spiketimes','stream','t','tarp','tau','tau_exc','tau_inh','tend',...
%         'tpost','tpre','tstim','ttran','y');
% end
