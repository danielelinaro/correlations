clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary
addpath ..

%%
%%% time constant of the cells
tau = [0.008,0.007];    % [s]
%%% balanced voltages
V_bal = [-41,-42];      % [mV]
% number of trials
ntrials = 100;

for id=3:8
    for c=0:0.1:1
        fprintf('ID = %d - c = %.2f.\n', id, c);
        simulateLIF(id, tau(mod(id,2)+1), V_bal(mod(id,2)+1), c, ntrials);
    end
end

%%
% T = 40e-3;
% files = dir('*.mat');
% for k=1:length(files)
%     load(files(k).name);
%     L = tstim - ttran;
%     [y,edges,n] = binarizeSpikeTrain(spiketimes,binWidth,[0,L],T);
%     save(files(k).name, 'C','E0','E_exc','E_inh','Er','Gm_exc','Gm_exc_c',...
%         'Gm_inh','Gm_inh_c','Gs_exc','Gs_exc_c','Gs_inh','Gs_inh_c','ID','L',...
%         'R_exc','Rm','T','V','V_bal','Vth','binWidth','c','cv','dt','edges',...
%         'isi','mode','n','ntrials','nu','rates','ratio','seed_exc_c','seed_inh_c',...
%         'spiketimes','stream','t','tarp','tau','tau_exc','tau_inh','tend',...
%         'tpost','tpre','tstim','ttran','y');
% end
