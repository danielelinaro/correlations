clear all;
close all;
clc;
addpath ~/MatlabLibrary;
addpath ~/MatlabLibrary/neuron_models;

%%
% Mono- and bi-exponential functions
exp_func = @(param,x) param(1)*exp(-x/param(2));
exp2_func = @(param,x) param(1)*exp(-x/param(2)) + param(3)*exp(-x/param(4));

% Model parameters
C = 0.0003;       % [?F]
gl = 0.03;        % [uS]
El = -65;         % [mV]
Er = -55;         % [mV]
tarp = 2e-3;      % [ms]
V0 = -45;         % [mV]
deltaV = 1;       % [mV]
Rm = 1/gl;        % [MOhm]

% Spike-triggered current parameters
eta1 = 0.05;      % [nA]
eta_tau1 = 0.2;   % [s]
eta2 = 0.1;       % [nA]
eta_tau2 = 0.02;  % [s]

% Moving threshold parameters
% double exponential model
% gamma1 = 0.5;       % [mV]
% gamma_tau1 = 0.15;  % [s]
% gamma2 = 1;         % [mV]
% gamma_tau2 = 0.045; % [s]
% single exponential model
gamma1 = 5;          % [mV]
gamma_tau1 = 0.02;   % [s]

% Parameters of the simulated background activity
mode = 'relative';
V_bal = linspace(-60,-10,51);
tau_exc = 5e-3;
tau_inh = 10e-3;
E_exc = 0;
E_inh = -80;
R_exc = 14000;
n_voltages = length(V_bal);

c = 0.5;

%%% Parameters of the simulation
n_trials = 20;
dt = 50e-6;     % [s] = 20 kHz sampling frequency
tpre = 0.2;     % [s]
tpost = 0.2;    % [s]
ttran = 0.2;    % [s]
L = 20;         % [s]
tstim = L+ttran;
tend = tstim + tpre + tpost;
t = 0:dt:tend;
eta = exp2_func([eta1, eta_tau1, eta2, eta_tau2], t);
% gamma = exp2_func([gamma1, gamma_tau1, gamma2, gamma_tau2], t);
gamma = exp_func([gamma1, gamma_tau1], t);
V = zeros(n_trials,length(t));
idx = find(t>tpre+ttran & t<=tpre+tstim);
t = (t(idx) - (tpre + ttran));

spike_times = cell(n_voltages,1);
firing_rates = cell(n_voltages,1);
y = cell(n_voltages,1);
bin_width = 2e-3;

threshold = 0;

%%% Simulate the neuron
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));
stream = RandStream('mt19937ar','Seed',5061983);
seed_exc_c = stream.randi(10000,n_trials,1);
seed_inh_c = stream.randi(20000,n_trials,1);

for i=1:n_voltages
    fprintf(1, 'Voltage [%03d/%03d]:\n   ', i, n_voltages);
    ratio = computeRatesRatio(mode,V_bal(i),Rm,tau_exc*1e3,tau_inh*1e3,E_exc,E_inh);
    for j=1:n_trials
        fprintf(1, '.');
        if mod(j,50) == 0
            fprintf(1, '\n');
            if j ~= n_trials
                fprintf(1, '   ');
            end
        end
        [Gm_exc_c,Gm_inh_c,Gs_exc_c,Gs_inh_c] = ...
            computeSynapticBackgroundCoefficients(mode,Rm,R_exc*c,ratio,tau_exc*1e3,tau_inh*1e3);
        [Gm_exc,Gm_inh,Gs_exc,Gs_inh] = ...
            computeSynapticBackgroundCoefficients(mode,Rm,R_exc*(1-c),ratio,tau_exc*1e3,tau_inh*1e3);
        Gm_exc_c = Gm_exc_c*1e-3;
        Gm_inh_c = Gm_inh_c*1e-3;
        Gs_exc_c = Gs_exc_c*1e-3;
        Gs_inh_c = Gs_inh_c*1e-3;
        Gm_exc = Gm_exc*1e-3;
        Gm_inh = Gm_inh*1e-3;
        Gs_exc = Gs_exc*1e-3;
        Gs_inh = Gs_inh*1e-3;
        Ge_c = [pre, OU_fast(tstim,dt,Gm_exc_c,Gs_exc_c,tau_exc,Gm_exc_c,seed_exc_c(j)), post];
        Gi_c = [pre, OU_fast(tstim,dt,Gm_inh_c,Gs_inh_c,tau_inh,Gm_inh_c,seed_inh_c(j)), post];
        Ge = [pre, OU_fast(tstim,dt,Gm_exc,Gs_exc,tau_exc,Gm_exc,randi(10000)), post];
        Gi = [pre, OU_fast(tstim,dt,Gm_inh,Gs_inh,tau_inh,Gm_inh,randi(10000)), post];
        V(j,:) = IF_eta_fast(tend,dt,C,gl,El,Er,tarp,V0,deltaV,eta,gamma,0,Ge_c,E_exc,Gi_c,E_inh,Ge,E_exc,Gi,E_inh);
    end
    if mod(j,50)
        fprintf('\n');
    end
    spike_times{i} = extractAPPeak(t,V(:,idx),threshold);
    firing_rates{i} = cellfun(@(x) length(x)/L, spike_times{i});
    y{i} = binarizeSpikeTrain(spike_times{i},bin_width,[0,L]);
end
nu = cellfun(@(x) mean(x), firing_rates);
clear V Ge Gi Ge_c Gi_c i j t pre post ratio Gm_* Gs_* idx stream I tmp

%%
geom_mean_rate = [];
r = [];
c_ii = zeros(n_voltages,1);
delta = 30*bin_width;
fprintf(1, 'Computing autocorrelations:\n   ');
for i=1:n_voltages
    fprintf(1,'+');
    [~,~,c_ii(i)] = computeRho(y{i},y{i},L,bin_width,delta,firing_rates{i},firing_rates{i});
end
fprintf(1,'\n');
cnt = 1;
fprintf(1, 'Computing rho:\n   ');
for i=1:n_voltages
    for j=i+1:n_voltages
        fprintf(1,'.');
        if mod(cnt,100) == 0
            fprintf(1,'\n   '); 
        end
        if nu(i)/nu(j) >= 0.5 && nu(i)/nu(j) <= 2
            geom_mean_rate = [geom_mean_rate; sqrt(prod(nu([i,j])))];
            r = [r; computeRho(y{i},y{j},L,bin_width,delta,firing_rates{i},...
                firing_rates{j},c_ii(i),c_ii(j))];
        end
        cnt = cnt+1;
    end
end
fprintf(1,'\n');

clear i j cnt
suffix = datestr(now,'yyyymmddHHMMSS');
save(['IF_eta_correlations_',suffix,'.mat']);

%%
idx = find(~imag(r));
geom_mean_rate = geom_mean_rate(idx);
r = r(idx);
edges = (1:3:110)';
n = histc(geom_mean_rate,edges);
rm = zeros(length(edges)-1,1);
rs = zeros(length(edges)-1,1);
for i=1:length(edges)-1
    idx = find(geom_mean_rate>=edges(i) & geom_mean_rate<edges(i+1));
    rm(i) = mean(r(idx));
    rs(i) = std(r(idx));
end
edges = edges(1:end-1) + diff(edges)/2;

black = [0,0,0];
white = [1,1,1];
red = [1,0,0];
light = [.5,.5,.5];

figure;
hold on;
plot(geom_mean_rate,r,'o','Color',[.6,.6,.6],'MarkerFaceColor',[.6,.6,.6],...
    'MarkerSize',4);
errorbar(edges,gclamp_ahp.rm,gclamp_ahp.rs,'ko','MarkerFaceColor','k','MarkerSize',6);
axis([0,30,0,0.5]);
set(gca,'XTick',0:5:30,'YTick',0:.1:.5);
xlabel('Geometric mean rate');
ylabel('Correlation');
set(gcf,'PaperUnits','Inch','PaperPosition',[0,0,8,5]);
% print('-depsc2',['IF_eta_correlations_',suffix,'.eps']);

