clear all;
close all;
clc;
addpath ~/MatlabLibrary;
addpath ~/MatlabLibrary/neuron_models;

%%
C = 0.08;       % [nF]
tau = 0.0075;   % [s] default value, gives an input resistance of 93.75 MOhm
tarp = 0.0014;  % [s]
Er = -65.2;     % [mV]
E0 = -70;       % [mV]
Vth = -50;      % [mV]
Rm = tau/C*1e3; % [mOhm]

Im = linspace(150,300,20);
% Is = logspace(log10(50),log10(250),7);
% Im = 300;
Is = 350;
c = 0.5;

tmp = repmat(Is(:)',[length(Im),1]);
pars = [repmat(Im(:),[length(Is),1]), tmp(:)];
n_pars = size(pars,1);

%%% Parameters of the simulation
n_trials = 100;
dt = 50e-6;     % [s] = 20 kHz sampling frequency
tpre = 0.5;     % [s]
tpost = 0.1;    % [s]
ttran = 1;      % [s]
L = 100;         % [s]
tstim = L+ttran;      % [s]
tend = tstim + tpre + tpost;
t = 0:dt:tend;
tstim_steps = round(tstim/dt) + 1;
V = zeros(n_trials,length(t));
idx = find(t>tpre+ttran & t<=tpre+tstim);
t = t(idx) - (tpre + ttran);

spike_times = cell(n_pars,1);
firing_rates = cell(n_pars,1);
y = cell(n_pars,1);
bin_width = tarp;

%%% Simulate the neuron
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));
stream = RandStream('mt19937ar','Seed',5061983);
seeds = stream.randi(10000,n_trials,1);

for i=1:n_pars
    fprintf(1, '[%03d/%03d] Im = %f, Is = %f:\n   ', i, n_pars, pars(i,1), pars(i,2));
    for j=1:n_trials
        fprintf(1, '.');
        if mod(j,50) == 0
            fprintf(1, '\n');
            if j ~= n_trials
                fprintf(1, '   ');
            end
        end
        rnd = RandStream('mt19937ar','Seed',seeds(j));
        I = [pre, pars(i,1) + pars(i,2) * ...
            (sqrt(1-c) * randn(1,tstim_steps) + ...
             sqrt(c) * rnd.randn(1,tstim_steps)), post];
        V(j,:) = LIF_fast(tend,dt,C,tau,tarp,Er,E0,Vth,I);
    end
    if mod(j,50)
        fprintf('\n');
    end
    spike_times{i} = extractAPPeak(t,V(:,idx),0);
    firing_rates{i} = cellfun(@(x) length(x)/L, spike_times{i});
    y{i} = binarizeSpikeTrain(spike_times{i},bin_width,[0,L]);
end
nu = cellfun(@(x) mean(x), firing_rates);

clear V Ge Gi Ge_c Gi_c i j t pre post ratio Gm_* Gs_* idx stream I tmp

%%
geom_mean_rate = [];
r = [];
c_ii = zeros(n_pars,1);
delta = 70*bin_width;
fprintf(1, 'Computing autocorrelations:\n   ');
for i=1:n_pars
    fprintf(1,'+');
    [~,~,c_ii(i)] = computeRho(y{i},y{i},L,bin_width,delta,firing_rates{i},firing_rates{i});
end
fprintf(1,'\n');
cnt = 1;
fprintf(1, 'Computing rho:\n   ');
for i=1:n_pars
    for j=i+1:n_pars
        fprintf(1,'.');
        if mod(cnt,100) == 0
            fprintf(1,'\n   '); 
        end
        if nu(i)/nu(j) >= 0.5 && nu(i)/nu(j) <= 2
            geom_mean_rate = [geom_mean_rate; sqrt(prod(nu([i,j])))];
            r = [r; computeRho(y{i},y{j},L,bin_width,delta,firing_rates{i},...
                firing_rates{j},c_ii(i),c_ii(j))];
        end
        cnt = cnt+1;
    end
end
fprintf(1,'\n');

clear i j cnt
save(sprintf('LIF_correlations_cc_delta_%s.mat',datestr(now,'yyyymmddHHMMSS')));

% %%
% figure;
% hold on;
% plot(geom_mean_rate,voltage_corr,'ko');
% plot(geom_mean_rate,current_corr,'ro');
% plot(geom_mean_rate,spike_count_corr,'s','Color',[1,.5,0]);
% axis([0,25,0,0.5]);
% hndl = legend('V_m correlation','I correlation','\rho_{ij}','Location','SouthEast');
% set(hndl,'box','off');
% xlabel('Geometric mean rate (Hz)');
% ylabel('Correlation');
% set(gca,'XTick',0:5:20,'YTick',0:.1:.5);
% set(gcf,'Color','w','PaperPosition',[0,0,8,5],'PaperUnits','Inch');
% print('-depsc2','LIF_correlations_instantaneous_decay.eps');
% 
% %%
% window_duration = 1;      % [s]
% window_overlap = 0.5;     % [s]
% window_duration_samples = round(window_duration/dt);
% window_overlap_samples = round(window_overlap/dt);
% 
% ndx = 1:window_duration_samples;
% offset = 0;
% 
% T = window_duration/2 : window_overlap : t(end)-window_duration/2;
% C = zeros(size(T));
% i = 1;
% for offset=0:window_overlap_samples:length(I)-window_duration_samples
%     x = I(:,offset+ndx);
%     C(i) = corr(I(1,offset+ndx)',I(2,offset+ndx)');
%     i = i+1;
% end
% 
% %%
% spks = extractAPPeak(t,V,0);
% isi = cellfun(@(x) diff(x), spks, 'UniformOutput', 0);
% F = cellfun(@(x) 1./x, isi, 'UniformOutput', 0);
% X = cell(n_pars,1);
% binned_F = cell(n_pars,1);
% for i=1:n_pars
%     j = 1;
%     for offset=0:window_overlap_samples:length(I)-window_duration_samples
%         idx = find(spks{i} > t(offset+ndx(1)) & spks{i} < t(offset+ndx(end)));
%         binned_F{i}(j) = length(idx) / window_duration;
%         j = j+1;
%     end
% end
% 
% %%
% window = [10e-3,10e-3];
% displacement = [-0.1 : 0.01 : 0.1, -0.03 : 0.002 : 0.03];
% displacement = sort(unique(displacement));
% n_disp = length(displacement);
% FF = zeros(size(displacement));
% for i=1:n_disp
%     fprintf(1, '[%02d/%02d]\n', i, n_disp);
%     [~,Vwndw] = ETSA(t,V(2,:),spks{1}(2:end-1)+displacement(i),window);
%     Twndw = (0:size(Vwndw,2)-1) * dt;
%     spks_window = extractAPPeak(Twndw,Vwndw,0);
%     FF(i) = sum(cellfun(@(x) length(x), spks_window)) / (length(spks_window)*sum(window));
% end
% %%
% plot(displacement, FF-length(spks{2})/t(end), 'k-o');
% 
% %%
% figure;
% hold on;
% plot(t,V(1,:),'k');
% % plot(T,binned_F{1},'m');
% plot(T,C*100,'b');
