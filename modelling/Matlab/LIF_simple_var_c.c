#include "mex.h"
#include <math.h>

#define IS_SCALAR(x) (mxGetM(x) == 1 && mxGetN(x) == 1)

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
        double tend, dt, taum, threshold, mu, sigma, c0, dc, tauc, *xii, *xic;
        double reset=0., coeff[2];
        double *V, *I, *c;
        size_t i, n;

        /* check that the number of inputs and outputs is correct */
        if (nrhs != 11)
                mxErrMsgTxt("usage: [V,I,c] = LIF_simple_var_c(tend, dt, taum, threshold, mu, sigma, c0, dc, tauc, xii, xic)");

        /* simulation parameters */
        tend      = mxGetScalar(prhs[0]);
        dt        = mxGetScalar(prhs[1]);
        /* model parameters */
        taum      = mxGetScalar(prhs[2]);
        threshold = mxGetScalar(prhs[3]);
        /* current means and standard deviations */
        mu        = mxGetScalar(prhs[4]);
        sigma     = mxGetScalar(prhs[5]);
        /* correlation parameters */
        c0        = mxGetScalar(prhs[6]);
        dc        = mxGetScalar(prhs[7]);
        tauc      = mxGetScalar(prhs[8]);

        /* number of steps */
        n = (size_t) round(tend/dt) + 1;

        /* random numbers */
        if (mxGetM(prhs[9]) != 1 && mxGetN(prhs[9]) != n)
            mxErrMsgTxt("Wrong number of elements in array xii.\n");
        xii  = mxGetData(prhs[9]);
        if (mxGetM(prhs[10]) != 1 && mxGetN(prhs[10]) != n)
            mxErrMsgTxt("Wrong number of elements in array xic.\n");
        xic  = mxGetData(prhs[10]);
        
        /* create the output vectors and assign them to the corresponding variables */
        plhs[0] = mxCreateDoubleMatrix(1, n, mxREAL);
        V = mxGetData(plhs[0]);
        plhs[1] = mxCreateDoubleMatrix(1, n, mxREAL);
        I = mxGetData(plhs[1]);
        plhs[2] = mxCreateDoubleMatrix(1, n, mxREAL);
        c = mxGetData(plhs[2]);

        /* helper value */
        coeff[0] = dt/taum;
        coeff[1] = sqrt(2*taum/dt);
        /* initial condition */
        V[0] = reset;
        I[0] = mu;
        c[0] = c0;
        
        /* main loop */
        for (i=1; i<n; i++) {
            c[i] = c[i-1] + dt/tauc * (c0 - c[i-1]);
            /* compute the total current */
            I[i] = mu + sigma * coeff[1] * (sqrt(1-c[i]) * xii[i] + sqrt(c[i])*xic[i]);
            V[i] = V[i-1] + coeff[0]*(I[i-1] - V[i-1]);
            if (V[i] > threshold) {
                c[i] -= dc;
                if (c[i] < 0)
                    c[i] = 0;
                i++;
                V[i] = reset;
                c[i] = c[i-1] + dt/tauc * (c0 - c[i-1]);
            }
        }
}

