clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary
addpath ../..

%%
%%% input resistance of the cells
Rm = 25:5:60;
%%% leak conductances of the cells
gL = 1e3./Rm;
%%% balanced voltages
V_bal = linspace(-37.5,-29.5,8);
% number of trials
ntrials = 100;

for id=1:length(gL)
    for c=0:0.1:1
        fprintf('ID = %d - c = %.2f.\n', id, c);
        simulateaEIF(id, gL(id), V_bal(id), c, ntrials);
    end
end
