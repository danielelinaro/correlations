clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary
addpath ../..

%%
% Compute spike train correlation as a function of input correlation.

ID = 1:8;
V_bal = linspace(-37.5,-29.5,8);
ncells = length(ID);
C_input = 0:0.1:1;
n = length(C_input);
C_output = zeros(ncells*(ncells-1)/2,n);
cnt = 1;
for i=1:ncells
    for j=i+1:ncells
        for k=1:n
            A = load(sprintf('correlations_neuron_%d_V_%.6e_c_%.2f_1.mat',...
                ID(i),V_bal(i),C_input(k)),'n','L','T');
            B = load(sprintf('correlations_neuron_%d_V_%.6e_c_%.2f_1.mat',...
                ID(j),V_bal(j),C_input(k)),'n');
            C_output(cnt,k) = computeCorrelationCoefficient({A.n},{B.n},A.L,A.T);
        end
        cnt = cnt+1;
    end
end

%%
figure;
hold on;
plot([0,1],[0,1],'--','Color',[.4,.4,.4],'LineWidth',2);
plot(C_input,C_output,'o-','Color',[.6,.6,.6],...
    'MarkerFaceColor',[.6,.6,.6],'MarkerSize',4);
plot(C_input,mean(C_output),'ks-','LineWidth',3);
axis([0,1,0,1]);
axis square;
xlabel('Input correlation c');
ylabel('Output correlation \rho_T');
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,5,5]);
print('-depsc2','c_vs_rhoT.eps');

