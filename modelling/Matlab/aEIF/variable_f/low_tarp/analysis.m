clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary
addpath ../..

%% 
% Compute spike train correlation as a function of the geometric mean
% firing rate of the two cells.

ID = 1:8;
ncells = length(ID);
nsteps = 10;
V_bal = zeros(ncells,nsteps);
V_bal(:,1) = linspace(-42,-35,ncells)';
V_bal(:,end) = linspace(-22,-5,ncells)';
for k=1:ncells
   V_bal(k,:) = linspace(V_bal(k,1),V_bal(k,end),nsteps);
end

NU = cell(ncells,1);
CV = cell(ncells,1);

geom_mean_rate = [];
C = [];
CV_pair = [];
for i=1:ncells
    stimulus_sets_i = dir(sprintf('correlations_neuron_%d_*_1.mat',ID(i)));
    for n=1:length(stimulus_sets_i)
        data_in = load(stimulus_sets_i(n).name,'n','nu','cv','L','T');
        NU{i} = [NU{i} ; data_in.nu];
        CV{i} = [CV{i} ; data_in.cv];
        for j=i+1:ncells
            stimulus_sets_j = dir(sprintf('correlations_neuron_%d_*_1.mat',ID(j)));
            for m=1:length(stimulus_sets_j)
                data_jm = load(stimulus_sets_j(m).name,'n','nu','cv');
                if data_in.nu/data_jm.nu > 0.5 && data_in.nu/data_jm.nu < 2
                    geom_mean_rate = [geom_mean_rate ; sqrt(data_in.nu*data_jm.nu)];
                    CV_pair = [CV_pair ; sqrt(data_in.cv*data_jm.cv)];
                    C = [C ; computeCorrelationCoefficient({data_in.n},...
                        {data_jm.n},data_in.L,data_in.T)];
                end
            end
        end
    end
end

%%% remove outliers
idx = find(C < 0.7);
geom_mean_rate = geom_mean_rate(idx);
C = C(idx);
CV_pair = CV_pair(idx);

%%
edges = 0:2:60;
[cnt,bin] = histc(geom_mean_rate,edges);

x = edges(1:end-1) + diff(edges(1:2))/2;
m = zeros(length(edges)-1,1);
s = zeros(length(edges)-1,1);
for k=1:length(edges)-1
    m(k) = mean(C(bin==k));
    s(k) = std(C(bin==k));% / sqrt(sum(bin==edges(k)));
end

[R,P] = corrcoef([CV_pair,C]);

%%
figure;
axes('Position',[0.15,0.1,0.7,0.55],'NextPlot','Add');
plot(geom_mean_rate, C, 'o', 'Color',[.6,.6,.6],...
    'MarkerFaceColor',[.6,.6,.6],'MarkerSize',4);
errorbar(x,m,s,'ks-','LineWidth',2);
xlabel('Geometric mean output rate (AP/s)');
ylabel('Output correlation \rho_T');
axis([0,50,0,0.5]);

axes('Position',[0.15,0.1,0.7,0.1],'NextPlot','Add');
bar(x,cnt(1:end-1),.8,'FaceColor',[.6,.6,1],'EdgeColor','None');
lim = ceil(max(cnt)/10)*10;
set(gca,'YAxisLocation','Right','YTick',0:lim/2:lim);
axis([0,50,0,lim]);
ylabel('Number of pairs');

axes('Position',[0.15,0.75,0.3,0.2],'NextPlot','Add');
cmap = jet(ncells);
for i=1:ncells
    plot(NU{i},CV{i},'o','Color',cmap(i,:),'MarkerFaceColor',cmap(i,:),...
        'MarkerSize',6);
end
plot([0,50],[1,1],'--','Color',[.6,.6,.6],'LineWidth',2);
axis([0,50,0,1.2]);
set(gca,'XTick',0:25:50,'YTick',0:0.6:1.2);
xlabel('\nu_i (AP/s)');
ylabel('CV_i');

axes('Position',[0.55,0.75,0.3,0.2],'NextPlot','Add');
plot(CV_pair,C,'o', 'Color',[.6,.6,.6],...
    'MarkerFaceColor',[.6,.6,.6],'MarkerSize',4);
% title(sprintf('R = %.3f - p = %.3f', R(2), P(2)));
axis([0,1.2,0,0.4]);
set(gca,'XTick',0:.6:1.2,'YTick',0:0.2:0.4);
xlabel('Sqrt(CV_i CV_j)');
ylabel('\rho_T');

set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,7,5]);
print('-depsc2','c_vs_rate.eps');
