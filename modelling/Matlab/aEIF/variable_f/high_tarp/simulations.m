clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary
addpath /home/daniele/projects/correlations/modelling

%%
%%% input resistance of the cells
Rm = 25:5:60;
%%% leak conductances of the cells
gL = 1e3./Rm;
%%% refractory period
tarp = 0.01;
%%% balanced voltages
ID = 1:length(gL);
nID = length(ID);
nsteps = 10;
V_bal = zeros(nID,nsteps);
V_bal(:,1) = linspace(-42,-35,nID)';
V_bal(:,end) = linspace(-22,-5,nID)';
% number of trials
ntrials = 100;
% correlation coefficient
c = 0.5;

for id=1:nID
    V_bal(id,:) = linspace(V_bal(id,1),V_bal(id,end),nsteps);
    V_bal(id,:) = V_bal(id,:) + (-1+2*rand(1,nsteps));
    for k=1:nsteps
        fprintf('ID = %d - V_bal = %.2f.\n', id, V_bal(id,k));
        simulateaEIF(id, gL(id), V_bal(id,k), tarp, c, ntrials);
    end
end
