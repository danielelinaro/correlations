clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary;

%%
L = 100;    % [s]
n_trials = 100;
rates = [0.1:.2:1.3,1.5:.5:4,5:20];  % [Hz]
bin_width = 1.4e-3;
n_rates = length(rates);
spike_times = cell(n_rates,1);
firing_rates = cell(n_rates,1);
y = cell(n_rates,1);
for i=1:length(rates)
    spike_times{i} = cell(n_trials,1);
    for j=1:n_trials
        r = rates(i)*0.95 + 0.1*rates(i)*rand;
        isi = - log(rand(1,round(r*L))) / r;
        spike_times{i}{j} = cumsum(isi);
        spike_times{i}{j} = spike_times{i}{j}(spike_times{i}{j} < L);
    end
    firing_rates{i} = cellfun(@(x) length(x)/L, spike_times{i});
    y{i} = binarizeSpikeTrain(spike_times{i},bin_width,[0,L]);
end
nu = cellfun(@(x) mean(x), firing_rates);

%%
geom_mean_rate = [];
r = [];
c_ii = zeros(n_rates,1);
delta = 70*bin_width;
fprintf(1, 'Computing autocorrelations:\n   ');
for i=1:n_rates
    fprintf(1,'+');
    [~,~,c_ii(i)] = computeRho(y{i},y{i},L,bin_width,delta,firing_rates{i},firing_rates{i});
end
fprintf(1,'\n');
cnt = 1;
fprintf(1, 'Computing rho:\n   ');
for i=1:n_rates
    for j=i+1:n_rates
        fprintf(1,'.');
        if mod(cnt,100) == 0
            fprintf(1,'\n   '); 
        end
        if nu(i)/nu(j) >= 0.5 && nu(i)/nu(j) <= 2
            geom_mean_rate = [geom_mean_rate; sqrt(prod(nu([i,j])))];
            r = [r; computeRho(y{i},y{j},L,bin_width,delta,firing_rates{i},...
                firing_rates{j},c_ii(i),c_ii(j))];
        end
        cnt = cnt+1;
    end
end
fprintf(1,'\n');

clear i j cnt
save(sprintf('poisson_correlations_%s.mat',datestr(now,'yyyymmddHHMMSS')));
