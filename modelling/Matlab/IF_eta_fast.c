#include "mex.h"
#include <math.h>
#include <time.h>

#define SPIKE
#ifdef SPIKE
#define VPEAK 30.0
#define SPIKE_LINEAR_DECAY
#endif

#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define IS_SCALAR(x) (mxGetM(x) == 1 && mxGetN(x) == 1)
#define LENGTH(x) MAX(mxGetM(x),mxGetN(x))

/*** mini-random library ***/
unsigned long long u,v,w;
unsigned long long int64() {
    unsigned long long x;
    u = u * 2862933555777941757LL + 7046029254386353087LL;
    v ^= v >> 17;
    v ^= v << 31;
    v ^= v >> 8;
    w = 4294957665U*(w & 0xffffffff) + (w >> 32);
    x = u ^ (u << 21); x ^= x >> 35; x ^= x << 4;
    return (x + v) ^ w;
}
unsigned int int32() {
    return (unsigned int) int64();
}
double doub() {
    return 5.42101086242752217E-20 * int64();
}
void set_seed(unsigned long long seed) {
    v = 4101842887655102017LL;
    w = 1;
    u = seed ^ v; int64();
    v = u; int64();
    w = v; int64();
}
/*** end of mini-random library ***/

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
        double tend, dt, C, gl, El, Er, tarp, V0, deltaV;
        double *eta, *gamma;
        double *Iext;
        double coeff, p, Itot;
        double **G, *E;
        double *V, *w, *Vt;
        size_t n, n_eta, n_gamma, refractory_period, n_conductances, i, j;
#ifdef SPIKE_LINEAR_DECAY
        double decay_coeff;
#endif 

        /* check that the number of inputs and outputs is correct */
        if (nrhs < 11)
                mxErrMsgTxt("usage: [V,w,Vt] = IF_eta_fast(tend, dt, C, g_l, E_l,"
                        "E_reset, t_refr, V0, DeltaV, eta, gamma, Iext, [G_1, E_1, [G_2, E_2, ...]])\n");

        set_seed(time(NULL));
        
        /* simulation parameters */
        tend   = mxGetScalar(prhs[0]);
        dt     = mxGetScalar(prhs[1]);
        /* model parameters */
        C      = mxGetScalar(prhs[2]);
        gl     = mxGetScalar(prhs[3]);
        El     = mxGetScalar(prhs[4]);
        Er     = mxGetScalar(prhs[5]);
        tarp   = mxGetScalar(prhs[6]);
        V0     = mxGetScalar(prhs[7]);
        deltaV = mxGetScalar(prhs[8]);

        /* the spike-triggered current */
        eta = mxGetData(prhs[9]);
        n_eta = LENGTH(prhs[9]);
        /* the moving threshold */
        gamma = mxGetData(prhs[10]);
        n_gamma = LENGTH(prhs[10]);
        
        /* number of steps */
        n = (size_t) round(tend/dt) + 1;

        if (nrhs > 11) {
                /* external applied current */
                if (IS_SCALAR(prhs[11])) {
                        /* the user gave just one value */
                        double tmp = mxGetScalar(prhs[11]);
                        Iext = mxCalloc(1, n*sizeof(double));
                        for (i=0; i<n; i++)
                                Iext[i] = tmp;
                }
                else {
                        /* the user gave the whole array */
                        if (LENGTH(prhs[11]) != n)
                                mxErrMsgTxt("Wrong number of elements in array Iext (%d, expected %d).\n", LENGTH(prhs[11]),n);
                        Iext = mxGetData(prhs[11]);
                }
                /* input conductances */
                n_conductances = (nrhs - 12)/2;
                if (n_conductances) {
                        G = (double **) mxCalloc(n_conductances,sizeof(double*));
                        E = (double *) mxCalloc(n_conductances,sizeof(double));
                        for (i=0; i<n_conductances; i++) {
                                if (IS_SCALAR(prhs[12+i*2])) {
                                        /* the user gave just one value */
                                        double tmp = mxGetScalar(prhs[12+i*2]);
                                        G[i] = mxCalloc(1, n*sizeof(double));
                                        for (j=0; j<n; j++)
                                                G[i][j] = tmp;
                                }
                                else {
                                        if (mxGetM(prhs[12+i*2]) != n && mxGetN(prhs[12+i*2]) != n)
                                                mxErrMsgTxt("Wrong number of elements in array G_%d.\n", i+1);
                                        G[i] = mxGetData(prhs[12+i*2]);
                                }
                                E[i] = mxGetScalar(prhs[13+i*2]);
                        }
                }
        }
        else {
                /* no external current, we set it to 0 */
                Iext = mxCalloc(1, n*sizeof(double));
                for (i=0; i<n; i++)
                        Iext[i] = 0.;
                /* no input conductances */
                n_conductances = 0;
        }

        /* create the output vectors and assign them to V, w and Vt, respectively */
        plhs[0] = mxCreateDoubleMatrix(1, n, mxREAL);
        plhs[1] = mxCreateDoubleMatrix(1, n, mxREAL);
        plhs[2] = mxCreateDoubleMatrix(1, n, mxREAL);
        V  = mxGetData(plhs[0]);
        w  = mxGetData(plhs[1]);
        Vt = mxGetData(plhs[2]);
        for (i=0; i<n; i++) {
            w[i] = 0;
            Vt[i] = 0;
        }
        /* some helper values */
        coeff = dt/C;
        refractory_period = (size_t) floor(tarp/dt);
#ifdef SPIKE_LINEAR_DECAY
        decay_coeff = dt/tarp * (VPEAK-Er);
#endif
        /* initial condition */
        V[0] = El;

        /* main loop */
        for (i=1; i<n; i++) {
            /* compute the total current I = Iext + g_i * (E_i - V) */
            Itot = Iext[i-1];
            for (j=0; j<n_conductances; j++)
                Itot += G[j][i-1] * (E[j] - V[i-1]);
            /* update the membrane voltage */
            V[i] = V[i-1] + coeff * (gl*(El-V[i-1]) + Itot - w[i-1]);
                
            /* probability of emitting a spike */
            p = exp(-exp((V[i]-(V0+Vt[i]))/deltaV));
               
            if (doub() > p) {
#ifdef SPIKE
                V[i] = VPEAK;
#endif
#ifndef SPIKE_LINEAR_DECAY
                /* clamp the membrane voltage to Er for the duration
                 * of the refractory period */
                for (j=0; j<refractory_period; j++)
                    V[++i] = Er;
#else
                /* linear decay of the membrane voltage to the
                 * reset voltage */
                for (j=0; j<refractory_period; j++) {
                    if (i == n-1)
                        return;
                    V[++i] = V[i-1] - decay_coeff;
                }
#endif
                for (j=0; j<MIN(n_eta,n-i); j++)
                    w[i+j+1] += eta[j];
                for (j=0; j<MIN(n_gamma,n-i); j++)
                    Vt[i+j+1] += gamma[j];

            }
        }
}

