#include "mex.h"
#include <math.h>

#define SPIKE
#ifdef SPIKE
#define VPEAK 20.0
#define SPIKE_LINEAR_DECAY
#endif

#define IS_SCALAR(x) (mxGetM(x) == 1 && mxGetN(x) == 1)

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
        double tend, dt, C, tau, tarp, Er, E0, Vth, *Iext;
        double coeff, Rl, Vinf, Itot;
        double **G, *E;
        double *V;
        size_t n, refractory_period, n_conductances, i, j;
#ifdef SPIKE_LINEAR_DECAY
        double decay_coeff;
#endif 

        /* check that the number of inputs and outputs is correct */
        if (nrhs < 8 || nlhs != 1)
                mxErrMsgTxt("usage: V = LIF_fast(tend, dt, C, tau, tarp, "
                        "Er, E0, Vth, Iext, [G_1, E_1, [G_2, E_2, ...]])\n");

        /* simulation parameters */
        tend = mxGetScalar(prhs[0]);
        dt   = mxGetScalar(prhs[1]);
        /* model parameters */
        C    = mxGetScalar(prhs[2]);
        tau  = mxGetScalar(prhs[3]);
        tarp = mxGetScalar(prhs[4]);
        Er   = mxGetScalar(prhs[5]);
        E0   = mxGetScalar(prhs[6]);
        Vth  = mxGetScalar(prhs[7]);

        /* number of steps */
        n = (size_t) round(tend/dt) + 1;

        if (nrhs > 8) {
                /* external applied current */
                if (IS_SCALAR(prhs[8])) {
                        /* the user gave just one value */
                        double tmp = mxGetScalar(prhs[8]);
                        Iext = mxCalloc(1, n*sizeof(double));
                        for (i=0; i<n; i++)
                                Iext[i] = tmp;
                }
                else {
                        /* the user gave the whole array */
                        if (mxGetM(prhs[8]) != n && mxGetN(prhs[8]) != n)
                                mxErrMsgTxt("Wrong number of elements in array Iext.\n");
                        Iext = mxGetData(prhs[8]);
                }
                /* input conductances */
                n_conductances = (nrhs - 9)/2;
                if (n_conductances) {
                        G = (double **) mxCalloc(n_conductances,sizeof(double*));
                        E = (double *) mxCalloc(n_conductances,sizeof(double));
                        for (i=0; i<n_conductances; i++) {
                                if (IS_SCALAR(prhs[9+i*2])) {
                                        /* the user gave just one value */
                                        double tmp = mxGetScalar(prhs[9+i*2]);
                                        G[i] = mxCalloc(1, n*sizeof(double));
                                        for (j=0; j<n; j++)
                                                G[i][j] = tmp;
                                }
                                else {
                                        if (mxGetM(prhs[9+i*2]) != n && mxGetN(prhs[9+i*2]) != n)
                                                mxErrMsgTxt("Wrong number of elements in array G_%d.\n", i+1);
                                        G[i] = mxGetData(prhs[9+i*2]);
                                }
                                E[i] = mxGetScalar(prhs[10+i*2]);
                        }
                }
        }
        else {
                /* no external current, we set it to 0 */
                Iext = mxCalloc(1, n*sizeof(double));
                for (i=0; i<n; i++)
                        Iext[i] = 0.;
                /* no input conductances */
                n_conductances = 0;
        }

        /* create the output vector and assign it to V */
        plhs[0] = mxCreateDoubleMatrix(1, n, mxREAL);
        V = mxGetData(plhs[0]);

        /* some helper values */
        coeff = exp(-1./tau*dt);
        Rl = tau/C;
        refractory_period = (size_t) floor(tarp/dt);
#ifdef SPIKE_LINEAR_DECAY
        decay_coeff = dt/tarp * (VPEAK-Er);
#endif
        /* initial condition */
        V[0] = E0;

        /* main loop */
        for (i=1; i<n; i++) {
                /* compute the total current I = Iext + g_i * (E_i - V) */
                Itot = Iext[i-1];
                for (j=0; j<n_conductances; j++)
                        Itot += G[j][i-1] * (E[j] - V[i-1]);
                /* theoretical steady state value */
                Vinf = Rl*Itot + E0;
                /* update the membrane voltage */
                V[i] = Vinf - coeff*(Vinf - V[i-1]);
                /* is the membrane voltage above threshold? */
                if (V[i] > Vth) {
#ifdef SPIKE
                        V[i] = VPEAK;
#endif
#ifndef SPIKE_LINEAR_DECAY
                        /* clamp the membrane voltage to Er for the duration
                         * of the refractory period */
                        for (j=0; j<refractory_period; j++)
                                V[++i] = Er;
#else
                        /* linear decay of the membrane voltage to the
                         * reset voltage */
                        for (j=0; j<refractory_period; j++) {
                                if (i == n-1)
                                        return;
                                V[++i] = V[i-1] - decay_coeff;
                        }
#endif
                }
        }
}

