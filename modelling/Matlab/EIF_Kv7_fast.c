#include "mex.h"
#include <math.h>

#define IS_SCALAR(x) (mxGetM(x) == 1 && mxGetN(x) == 1)

/*#define GX             0.3    /* [mS/cm^2] */
/*#define TAUX         200.     /* [ms] */
/*#define VXH         - 40.     /* [mV] */
/*#define DX             8.     /* [mV] */
/*#define EX          - 80.     /* [mV] */
/*#define ONE_OVER_DX    0.125  /* [mV^-1] */

#define VTH           20.     /* [mV] */
#define XINF(V, Vxh, Dx) (1. / (1. + exp(((Vxh)-(V))/(Dx))))

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
        double tend, dt, C, gl, El, deltaT, VT, Vr, tarp, gx, Vxh, Dx, taux, Ex, *Iext;
        double dt_over_C, gl_times_deltaT, one_over_deltaT, dt_over_taux, decay_coeff, Itot;
        double **G, *E;
        double *V, *x;
        size_t n, refractory_period, n_conductances, i, j;

        /* check that the number of inputs and outputs is correct */
        if (nrhs < 14)
                mxErrMsgTxt("usage: [V,m] = EIF_Kv7_fast(tend, dt, C, gL, EL, deltaT, "
                        "VT, Vr, tarp, gx, Vxh, Dx, taux, Ex, Iext, [G_1, E_1, [G_2, E_2, ...]])");

        /* simulation parameters */
        tend   = mxGetScalar(prhs[0]) * 1e3;
        dt     = mxGetScalar(prhs[1]) * 1e3;
        /* model parameters */
        C      = mxGetScalar(prhs[2]);
        gl     = mxGetScalar(prhs[3]);
        El     = mxGetScalar(prhs[4]);
        deltaT = mxGetScalar(prhs[5]);
        VT     = mxGetScalar(prhs[6]);
        Vr     = mxGetScalar(prhs[7]);
        tarp   = mxGetScalar(prhs[8]) * 1e3;
        gx     = mxGetScalar(prhs[9]);
        Vxh    = mxGetScalar(prhs[10]);
        Dx     = mxGetScalar(prhs[11]);
        taux   = mxGetScalar(prhs[12]) * 1e3;
        Ex     = mxGetScalar(prhs[13]);

        /* number of steps */
        n = (size_t) round(tend/dt) + 1;

        if (nrhs > 14) {
                /* external applied current */
                if (IS_SCALAR(prhs[14])) {
                        /* the user gave just one value */
                        double tmp = mxGetScalar(prhs[14]);
                        Iext = mxCalloc(1, n*sizeof(double));
                        for (i=0; i<n; i++)
                                Iext[i] = tmp;
                }
                else {
                        /* the user gave the whole array */
                        if (mxGetM(prhs[14]) != n && mxGetN(prhs[14]) != n)
                                mxErrMsgTxt("Wrong number of elements in array Iext.\n");
                        Iext = mxGetData(prhs[14]);
                }
                /* input conductances */
                n_conductances = (nrhs - 15)/2;
                if (n_conductances) {
                        G = (double **) mxCalloc(n_conductances,sizeof(double*));
                        E = (double *) mxCalloc(n_conductances,sizeof(double));
                        for (i=0; i<n_conductances; i++) {
                                if (IS_SCALAR(prhs[15+i*2])) {
                                        /* the user gave just one value */
                                        double tmp = mxGetScalar(prhs[15+i*2]);
                                        G[i] = mxCalloc(1, n*sizeof(double));
                                        for (j=0; j<n; j++)
                                                G[i][j] = tmp;
                                }
                                else {
                                        if (mxGetM(prhs[15+i*2]) != n && mxGetN(prhs[15+i*2]) != n)
                                                mxErrMsgTxt("Wrong number of elements in array G_%d.\n", i+1);
                                        G[i] = mxGetData(prhs[15+i*2]);
                                }
                                E[i] = mxGetScalar(prhs[16+i*2]);
                        }
                }
        }
        else {
                /* no external current, we set it to 0 */
                Iext = mxCalloc(1, n*sizeof(double));
                for (i=0; i<n; i++)
                        Iext[i] = 0.;
                /* no input conductances */
                n_conductances = 0;
        }

        /* create the output vector and assign it to V */
        plhs[0] = mxCreateDoubleMatrix(1, n, mxREAL);
        V = mxGetData(plhs[0]);
        if (nlhs > 1) {
                plhs[1] = mxCreateDoubleMatrix(1, n, mxREAL);
                x = mxGetData(plhs[1]);
        }
        else {
                x = mxCalloc(n, sizeof(double));
        }

        /* some helper values */
        dt_over_C = dt/C;
        dt_over_taux = 1./taux;
        gl_times_deltaT = gl*deltaT;
        one_over_deltaT = 1./deltaT;
        refractory_period = (size_t) floor(tarp/dt);
        decay_coeff = dt/tarp * (VTH-Vr);

        /* initial condition */
        V[0] = -65.;
        x[0] = 0.;

        /* main loop */
        for (i=1; i<n; i++) {
                /* compute the total current I = Iext + g_i * (E_i - V) */
                Itot = Iext[i-1];
                for (j=0; j<n_conductances; j++)
                        Itot += G[j][i-1] * (E[j] - V[i-1]);
                V[i] = V[i-1] + dt_over_C * (gl*(El-V[i-1]) + gx*x[i-1]*(Ex-V[i-1]) + 
                        gl_times_deltaT*exp(one_over_deltaT*(V[i-1]-VT)) + Itot);
                x[i] = x[i-1] + dt_over_taux*(XINF(V[i-1],Vxh,Dx) - x[i-1]);
                /* is the membrane voltage above threshold? */
                if (V[i] > VTH) {
                        V[i] = VTH;  
                        /* linear decay of the membrane voltage to the
                         * reset voltage */
                        for (j=0, i++; j<refractory_period; j++, i++) {
                                if (i == n)
                                        return;
                                V[i] = V[i-1] - decay_coeff;
                                x[i] = x[i-1] + dt_over_taux*(XINF(V[i-1],Vxh,Dx) - x[i-1]);
                        }
                        i--;
                }
        }
}

