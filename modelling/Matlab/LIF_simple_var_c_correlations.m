clear all;
close all;
clc;
addpath ~/MatlabLibrary;
addpath ~/MatlabLibrary/neuron_models;

%%
taum = 0.01;    % [s]
threshold = 20; % [mV]

% fixed mean and variable std
% mu = 10;                    % [mV]
% sigma = [4:.5:30, 31:50];   % [mV]
% fixed std and variable mean
% mu = 17:.5:32;      % [mV] - sigma = 1 mV
% mu = 14:31;      % [mV] - sigma = 2 mV
% mu = 10:.5:31;      % [mV] - sigma = 3 mV
mu = [6:.25:11, 12:.5:18, 19:31];      % [mV] - sigma = 4 mV
% mu = 4:31;      % [mV] - sigma = 5 mV
% mu = 2:31;      % [mV] - sigma = 6 mV
sigma = 4;

c0 = 0.5;
dc = 0.2;
tauc = 5e-3;

tmp = repmat(sigma(:)',[length(mu),1]);
pars = [repmat(mu(:),[length(sigma),1]), tmp(:)];
n_pars = size(pars,1);

%%% Parameters of the simulation
n_trials = 50;
dt = 50e-6;     % [s] = 20 kHz sampling frequency
ttran = 1;      % [s]
L = 100;        % [s]
tend = L+ttran;
t = 0:dt:tend;
V = zeros(n_trials,length(t));
idx = find(t>ttran);
t = t(idx) - ttran;

spike_times = cell(n_pars,1);
firing_rates = cell(n_pars,1);
y = cell(n_pars,1);
bin_width = 2e-3;

%%% Simulate the neuron
n = size(V,2);
stream = RandStream('mt19937ar','Seed',5061983);
seeds = stream.randi(10000,n_trials,1);

for i=1:n_pars
    fprintf(1, '[%03d/%03d] mu = %f, sigma = %f:\n   ', i, n_pars, pars(i,1), pars(i,2));
    for j=1:n_trials
        fprintf(1, '.');
        if mod(j,50) == 0
            fprintf(1, '\n');
            if j ~= n_trials
                fprintf(1, '   ');
            end
        end
        rnd = RandStream('mt19937ar','Seed',randi(10000));
        rnd_c = RandStream('mt19937ar','Seed',seeds(j));
        xii = rnd.randn(1,n);
        xic = rnd_c.randn(1,n);
        V(j,:) = LIF_simple_var_c(tend,dt,taum,threshold,pars(i,1),pars(i,2),c0,dc,tauc,xii,xic);
    end
    if mod(j,50)
        fprintf('\n');
    end
    spike_times{i} = extractAPPeak(t,V(:,idx),threshold);
    firing_rates{i} = cellfun(@(x) length(x)/L, spike_times{i});
    y{i} = binarizeSpikeTrain(spike_times{i},bin_width,[0,L]);
end
nu = cellfun(@(x) mean(x), firing_rates);
clear V i j t idx stream rnd xii xic 

%%
geom_mean_rate = [];
r = [];
c_ii = zeros(n_pars,1);
delta = 100*bin_width;
fprintf(1, 'Computing autocorrelations:\n   ');
for i=1:n_pars
    fprintf(1,'+');
    [~,~,c_ii(i)] = computeRho(y{i},y{i},L,bin_width,delta,firing_rates{i},firing_rates{i});
end
fprintf(1,'\n');
cnt = 1;
fprintf(1, 'Computing rho:\n   ');
for i=1:n_pars
    for j=i+1:n_pars
        fprintf(1,'.');
        if mod(cnt,100) == 0
            fprintf(1,'\n   '); 
        end
        if nu(i)/nu(j) >= 0.5 && nu(i)/nu(j) <= 2
            geom_mean_rate = [geom_mean_rate; sqrt(prod(nu([i,j])))];
            r = [r; computeRho(y{i},y{j},L,bin_width,delta,firing_rates{i},...
                firing_rates{j},c_ii(i),c_ii(j))];
        end
        cnt = cnt+1;
    end
end
fprintf(1,'\n');

clear i j cnt
suffix = datestr(now,'yyyymmddHHMMSS');
save(['LIF_simple_var_c_correlations_',suffix,'.mat']);

%%
idx = find(~imag(r));
geom_mean_rate = geom_mean_rate(idx);
r = r(idx);
edges = (1:3:110)';
n = histc(geom_mean_rate,edges);
rm = zeros(length(edges)-1,1);
rs = zeros(length(edges)-1,1);
for i=1:length(edges)-1
    idx = find(geom_mean_rate>=edges(i) & geom_mean_rate<edges(i+1));
    rm(i) = mean(r(idx));
    rs(i) = std(r(idx));
end
edges = edges(1:end-1) + diff(edges)/2;

figure;
hold on;
plot(geom_mean_rate,r,'o','Color',[.6,.6,.6],'MarkerFaceColor',[.6,.6,.6],...
    'MarkerSize',4);
errorbar(edges,rm,rs,'ko','MarkerFaceColor','k','MarkerSize',6);
axis([0,100,0,0.5]);
set(gca,'XTick',0:20:100,'YTick',0:.1:.5);
xlabel('Geometric mean rate');
ylabel('Correlation');
set(gcf,'PaperUnits','Inch','PaperPosition',[0,0,8,5]);
print('-depsc2',['LIF_simple_var_c_correlations_',suffix,'.eps']);

