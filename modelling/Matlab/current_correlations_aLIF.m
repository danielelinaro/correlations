clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary;

%%
C = 281;        % [pF]   membrane capacitance
gL = 20;        % [nS]   leak conductance
EL = -70.6;     % [mV]   leak reversal potential
VT = -50.4;     % [mV]   spike threshold
tauw = 0.01;    % [s]    adaptation time constant
a = 0;          % [nS]   subthreshold adaptation
b = 500;        % [pA]   spike-triggered adaptation
Vr = -55;       % [mV]   reset voltage
tarp = 2e-3;    % [s]    refractory period
Iext = 0;       % [pA]   externally applied current
Rm = 1e3/gL;    % [MOhm] input resistance

% Parameters of the simulated background activity
mode = 'relative';
V_bal = [-45.5,-41.5,-39,-34.5];
% V_bal = V_bal(end);
tau_exc = 5e-3;
tau_inh = 10e-3;
E_exc = 0;
E_inh = -80;
R_exc = 7000;
n_voltages = length(V_bal);

c = 0.5;

%%% Parameters of the simulation
n_trials = 2;
dt = 50e-6;     % [s] = 20 kHz sampling frequency
tpre = 0.2;     % [s]
tpost = 0.2;    % [s]
ttran = 0.2;    % [s]
L = 100;        % [s]
tstim = L+ttran;
tend = tstim + tpre + tpost;
t = 0:dt:tend;
V = zeros(n_trials,length(t));
I = zeros(n_trials,length(t));
idx = find(t>tpre+ttran & t<=tpre+tstim);
T = (t(idx) - (tpre + ttran));

window = [0:.25e-3:tarp, tarp+1e-3:5e-3:30e-3];
% window = window(1);
Icorr = zeros(n_voltages,length(window));
    
spike_times = cell(n_voltages,1);
firing_rates = cell(n_voltages,1);

%%% Simulate the neuron
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));
seed_exc_c = 5061983;
seed_inh_c = 70583;

for i=1:n_voltages
    fprintf(1, 'Voltage [%03d/%03d]:\n   ', i, n_voltages);
    ratio = computeRatesRatio(mode,V_bal(i),Rm,tau_exc*1e3,tau_inh*1e3,E_exc,E_inh);
    for j=1:n_trials
        fprintf(1, '.');
        if mod(j,50) == 0
            fprintf(1, '\n');
            if j ~= n_trials
                fprintf(1, '   ');
            end
        end
        [Gm_exc_c,Gm_inh_c,Gs_exc_c,Gs_inh_c] = ...
            computeSynapticBackgroundCoefficients(mode,Rm,R_exc*c,ratio,tau_exc*1e3,tau_inh*1e3);
        [Gm_exc,Gm_inh,Gs_exc,Gs_inh] = ...
            computeSynapticBackgroundCoefficients(mode,Rm,R_exc*(1-c),ratio,tau_exc*1e3,tau_inh*1e3);
        Ge_c = [pre, OU_fast(tstim,dt,Gm_exc_c,Gs_exc_c,tau_exc,Gm_exc_c,seed_exc_c), post];
        Gi_c = [pre, OU_fast(tstim,dt,Gm_inh_c,Gs_inh_c,tau_inh,Gm_inh_c,seed_inh_c), post];
        Ge = [pre, OU_fast(tstim,dt,Gm_exc,Gs_exc,tau_exc,Gm_exc,randi(10000)), post];
        Gi = [pre, OU_fast(tstim,dt,Gm_inh,Gs_inh,tau_inh,Gm_inh,randi(10000)), post];
        V(j,:) = aLIF_fast(tend,dt,C,gL,EL,VT,tauw,a,b,Vr,tarp,Iext,Ge_c,...
            E_exc,Gi_c,E_inh,Ge,E_exc,Gi,E_inh);
        I(j,:) = (Ge+Ge_c) .* (E_exc-V(j,:)) + (Gi+Gi_c) .* (E_inh-V(j,:));
    end
    if mod(j,50)
        fprintf('\n   ');
    end
    spike_times{i} = extractAPPeak(T,V(:,idx),VT);
    firing_rates{i} = cellfun(@(x) length(x)/L, spike_times{i});

    Vorig = V(:,idx);
    Iorig = I(:,idx);
    for j=1:length(window)
        fprintf(1, '.');
        Vtrimmed = trimSpikes(T,Vorig,{'threshold','threshold'},[0e-3,window(j)],0,'nan');
        tmp = corr(Iorig(:,~isnan(Vtrimmed(1,:)) & ~isnan(Vtrimmed(2,:)))');
        Icorr(i,j) = tmp(2);
    end
    fprintf(1, '\n');
  
end
nu = cellfun(@(x) mean(x), firing_rates);

suffix = datestr(now,'yyyymmddHHMMSS');
save(['current_correlations_aLIF_',suffix,'.mat'],'C','EL','E_exc','E_inh',...
    'Icorr','Iext','L','R_exc','Rm','VT','V_bal','Vr','a','b','c','dt',...
    'firing_rates','gL','mode','n_trials','n_voltages','nu','seed_exc_c',...
    'seed_inh_c','spike_times','tarp','tau_exc','tau_inh','tauw','tend',...
    'tpost','tpre','tstim','ttran','window');

%%
hndl = zeros(n_voltages,1);
lgnd = cell(n_voltages,1);
col = [1,.5,0 ; 0,1,.5 ; 1,0,0.5 ; 0,.5,1];
figure;
axes('Position',[0.1,0.1,0.5,0.85],'NextPlot','Add');
plot([0,30],[c,c],'--','Color',[.3,.3,.3],'LineWidth',2);
plot(tarp*[1e3,1e3],[0,c+0.05],'Color',[.75,.75,.75],'LineWidth',2);
for i=1:n_voltages
    hndl(i) = plot(window*1e3,Icorr(i,:),'s-','Color',col(i,:),...
        'MarkerFaceColor',col(i,:),'LineWidth',1);
    lgnd{i} = sprintf('nu = %g Hz', nu(i));
end
text(30,c+0.03,sprintf('c = %g',c),'HorizontalAlignment','Right',...
    'VerticalAlignment','Top');
text(tarp*1e3+1,c+0.03,sprintf('tau_{arp} = %g ms',tarp*1e3),...
    'HorizontalAlignment','Left','VerticalAlignment','Top');
xlabel('\Delta t (ms)');
ylabel('Current correlation');
h = legend(hndl,lgnd,'Location','SouthEast');
set(h,'Box','Off');
box off;
set(gca,'TickDir','out','XTick',0:5:30,'YTick',0:.1:.5);
axis([0,30,0,0.55]);

axes('Position',[0.65,0.1,0.32,0.85],'NextPlot','Add');
spk = find(diff(spike_times{end}{1}) > 80e-3,1);
idx = find(T > spike_times{end}{1}(spk)-20e-3 & T < spike_times{end}{1}(spk)+60e-3);
plot(T(idx),Vorig(1,idx),'k');
placeScaleBars(T(idx(1)),-50,[],5,'','5 mV','k');
placeScaleBars(T(idx(1)),min(Vorig(1,idx)),20e-3,[],'20 ms','','k');
axis([T(idx([1,end]))+[-1e-3,1e-3],min(Vorig(1,idx)),-40]);
axis off;

axes('Position',[0.8,0.6,0.15,0.35],'NextPlot','Add');
idx = find(T > spike_times{end}{1}(spk)-2e-3 & T < spike_times{end}{1}(spk)+5e-3);
plot(T(idx),Vorig(1,idx),'k');
placeScaleBars(T(idx(end)),-50,[],5,'','5 mV','k');
placeScaleBars(spike_times{end}{1}(spk),min(Vorig(1,idx))-2,tarp,[],...
    sprintf('%g ms',tarp*1e3),'','k');
axis([T(idx([1,end])),min(Vorig(1,idx))-3,-40]);
axis off;

set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,9,6]);
print('-depsc2',['current_correlations_aLIF_',suffix,'.eps']);

