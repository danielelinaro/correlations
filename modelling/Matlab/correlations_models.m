clear all;
close all;
clc;

%%
nap = load('EIF_Nap_correlations_20140312003211.mat');
kv7 = load('EIF_Kv7_correlations_20140312123741.mat');
lif = load('LIF_correlations_20140311152553.mat');
cc = load('LIF_simple_correlations_cc_20140523141156.mat');

%%
figure;
hold on;
plot(lif.geom_mean_rate,lif.r/0.5,'ko');
plot(nap.geom_mean_rate,nap.r/0.5,'ro');
plot(kv7.geom_mean_rate,kv7.r/0.5,'go');
plot(cc.geom_mean_rate,cc.r/0.5,'bs');
axis([0,30,0,1]);
