function simulateaEIF(ID, gL, V_bal, c, ntrials)

%%% Parameters of the neuron
C = 281;        % [pF] membrane capacitance
% gL = 30;        % [nS] leak conductance
EL = -70.6;     % [mV] leak reversal potential
VT = -50.4;     % [mV] spike threshold
deltaT = 2;     % [mV] slope factor
tauw = 0.144;   % [s] adaptation time constant
a = 4;          % [nS] subthreshold adaptation
b = 0.0805;     % [nA] spike-triggered adaptation
Vr = -55;       % [mV] reset voltage
tarp = 0.01;    % [s] refractory period
Rm = 1e3/gL;

%%% Parameters of the simulated background activity
mode = 'relative';
tau_exc = 5e-3;
tau_inh = 10e-3;
E_exc = 0;
E_inh = -80;
R_exc = 7000;

%%% Parameters of the simulation
dt = 1/20000;   % [s]
tstim = 1.1;    % [s]
tpre = 0.1;     % [s]
tpost = 0.1;    % [s]
ttran = 0.1;    % [s]
tend = tstim + tpre + tpost;
t = 0:dt:tend;
n = length(t);
ratio = computeRatesRatio(mode,V_bal,Rm,tau_exc*1e3,tau_inh*1e3,E_exc,E_inh);
[Gm_exc_c,Gm_inh_c,Gs_exc_c,Gs_inh_c] = ...
    computeSynapticBackgroundCoefficients(mode,Rm,R_exc*c,ratio,tau_exc*1e3,tau_inh*1e3);
[Gm_exc,Gm_inh,Gs_exc,Gs_inh] = ...
    computeSynapticBackgroundCoefficients(mode,Rm,R_exc*(1-c),ratio,tau_exc*1e3,tau_inh*1e3);

%%% Parameters of the analysis
L = tstim - ttran;
T = 40e-3;

%%% Simulate the neuron
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));
stream = RandStream('mt19937ar','Seed',5061983);
seed_exc_c = stream.randi(10000,ntrials);
seed_inh_c = stream.randi(20000,ntrials);

V = zeros(ntrials,n);
for k=1:ntrials
    Ge_c = struct('E', E_exc, 'G', [pre, ...
        OU(tstim,dt,Gm_exc_c,Gs_exc_c,tau_exc,seed_exc_c(k)), post]);
    Gi_c = struct('E', E_inh, 'G', [pre, ...
        OU(tstim,dt,Gm_inh_c,Gs_inh_c,tau_inh,seed_inh_c(k)), post]);
    Ge = struct('E', E_exc, 'G', [pre, ...
        OU(tstim,dt,Gm_exc,Gs_exc,tau_exc,randi(10000)), post]);
    Gi = struct('E', E_inh, 'G', [pre, ...
        OU(tstim,dt,Gm_inh,Gs_inh,tau_inh,randi(10000)), post]);
    V(k,:) = aEIF(tend,dt,C,gL,EL,deltaT,VT,tauw,a,b,Vr,tarp,0,Ge_c,Gi_c,Ge,Gi);
end

%%% Pre-analyse the data
idx = find(t >= tpre+ttran & t <= tpre+tstim);
spiketimes = extractAPPeak(t(idx)-t(idx(1)),V(:,idx),VT+10);
isi = [];
binWidth = 1.2e-3;
offset = 0;
for k=1:length(spiketimes)
    if isempty(spiketimes{k})
        offset = offset+1;
    else
        if isempty(isi)
            isi = spiketimes{k}(1);
        else
            isi = [isi, ...
                spiketimes{k}(1) + offset + (tstim-ttran) - last];
        end
        last = spiketimes{k}(1);
        if length(spiketimes{k}) > 1
            isi = [isi, ...
                diff(spiketimes{k})];
            last = spiketimes{k}(end);
        end
        offset = 0;
    end
end
rates = cellfun(@(x) length(x)/L, spiketimes);
[y,edges,n] = binarizeSpikeTrain(spiketimes,binWidth,[0,L],T);
cv = std(isi)/mean(isi);
nu = mean(rates);

%%% Save the data
clear Ge_c Gi_c Ge Gi pre post idx k last offset
cnt = 1;
while 1
    filename = sprintf('correlations_neuron_%d_V_%.2f_c_%.2f_%d.mat',ID,V_bal,c,cnt);
    if ~ exist(filename,'file')
        break;
    end
    cnt = cnt+1;
end
save(filename);
