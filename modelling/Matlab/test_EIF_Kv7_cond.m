clear all;
close all;
clc;

%%
pars = make_neuron_model_pars('EIF_Kv7');
pars.gx = 0;
mode = 'relative';
R_exc = 7000;
V_bal = -50;
tau_exc = 5e-3;
tau_inh = 10e-3;
E_exc = 0;
E_inh = -80;
tend = 1;
dt = 1/50000;
t = 0:dt:tend;
ratio = computeRatesRatio(mode,V_bal,pars.Rm,tau_exc*1e3,tau_inh*1e3,E_exc,E_inh);
[Gm_exc,Gm_inh,Gs_exc,Gs_inh] = ...
    computeSynapticBackgroundCoefficients(mode,pars.Rm,R_exc,ratio,tau_exc*1e3,tau_inh*1e3);
Ge = OU_fast(tend,dt,Gm_exc,Gs_exc,tau_exc,Gm_exc,randi(10000));
Gi = OU_fast(tend,dt,Gm_inh,Gs_inh,tau_inh,Gm_inh,randi(10000));
[V,x] = run_model('EIF_Kv7', tend, dt, pars, 0, Ge, E_exc, Gi, E_inh);
figure;
subplot(2,1,1);
plot(t,V,'k');
subplot(2,1,2);
hold on;
plot(t,Ge.*(E_exc-V),'r',t,Gi.*(E_inh-V),'b');
plot(t,Ge.*(E_exc-V)+Gi.*(E_inh-V),'k');
