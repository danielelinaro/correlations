#include "mex.h"
#include <math.h>

#define MM 714025
#define IA 1366
#define IC 150889

long rand49_idum = -77531;

float drand49()	{
        static long iy, ir[98];
        static int iff = 0;
        int j;

        if (rand49_idum < 0 || iff == 0) {
                iff=1;
                if((rand49_idum=(IC-rand49_idum) % MM)<0)
                        rand49_idum=(-rand49_idum);
                for (j=1;j<=97;j++) {
                        rand49_idum=(IA*(rand49_idum)+IC) % MM;
                        ir[j]=(rand49_idum);
                }
                rand49_idum=(IA*(rand49_idum)+IC) % MM;
                iy=(rand49_idum);
        }
        j=1 + 97.0*iy/MM;
        if (j > 97 || j < 1) fprintf(stderr, "RAN2: This cannot happen.");
        iy=ir[j];
        rand49_idum=(IA*(rand49_idum)+IC) % MM;
        ir[j]=(rand49_idum);
        return (float) iy/MM;
}

float srand49(long seed) {
        rand49_idum=(-seed);
        return drand49();
}

float gauss() {
        static int iset=0;
        static float gset;
        float fac,r,v1,v2;
        if (!iset) {
                r = 1.;
                while (r >= 1.) {
                        v1=2.0*drand49()-1.0;
                        v2=2.0*drand49()-1.0;
                        r=v1*v1+v2*v2;
                }
                fac=sqrt(-2.0*log(r)/r);
                gset=v1*fac;
                iset=1;
                return v2*fac;
        }
        iset=0;
        return gset;
}

#undef MM
#undef IA
#undef IC

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
        double tend, dt, mu, sigma, tau, eta0;
        double x, one_minus_x2, tau_over_2, c;
        double *eta;
        size_t n, i;

        /* check that the number of inputs and outputs is correct */
        if (nrhs < 5 || nlhs != 1)
                mxErrMsgTxt("usage: eta = OU(tend, dt, mu, sigma, tau, [eta0 = 0, [seed = time(NULL)]])\n");

        /* simulation parameters */
        tend  = mxGetScalar(prhs[0]);
        dt    = mxGetScalar(prhs[1]);
        /* OU parameters */
        mu    = mxGetScalar(prhs[2]);
        sigma = mxGetScalar(prhs[3]);
        tau   = mxGetScalar(prhs[4]);
        if (nrhs > 5)
                eta0 = mxGetScalar(prhs[5]);
        else
                eta0 = 0.0;
        if (nrhs > 6)
                srand49((long) mxGetScalar(prhs[6]));
        else
                srand49(time(NULL));


        /* number of steps */
        n = (size_t) round(tend/dt) + 1;

        /* create the output vector and assign it to eta */
        plhs[0] = mxCreateDoubleMatrix(1, n, mxREAL);
        eta = mxGetData(plhs[0]);

        /* some helper values */
        x = exp(-dt/tau);
        one_minus_x2 = 1 - x*x;
        tau_over_2 = tau/2;
        c = 2*sigma*sigma/tau;

        /* initial condition */
        eta[0] = eta0;

        for (i=1; i<n; i++)
                eta[i] = mu + x*(eta[i-1] - mu) + sqrt(c*tau_over_2*one_minus_x2) * gauss();
}

