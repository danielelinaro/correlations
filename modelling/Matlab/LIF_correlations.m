clear all;
close all;
clc;
addpath ~/MatlabLibrary;
addpath ~/MatlabLibrary/neuron_models;

%%
C = 0.08;       % [nF]
tau = 0.0075;   % [s] default value, gives an input resistance of 93.75 MOhm
tarp = 0.0014;  % [s]
Er = -65.2;     % [mV]
E0 = -70;       % [mV]
Vth = -50;      % [mV]
Rm = tau/C*1e3; % [mOhm]

%%% Parameters of the simulated background activity
mode = 'relative';
V_bal = linspace(-50,-40,31);
tau_exc = 5e-3;
tau_inh = 10e-3;
E_exc = 0;
E_inh = -80;
R_exc = 7000;
c = 0.5;
n_voltages = length(V_bal);
n_trials = 100;

%%% Parameters of the simulation
dt = 50e-6;     % [s] = 20 kHz sampling frequency
tpre = 0.5;     % [s]
tpost = 0.1;    % [s]
ttran = 1;      % [s]
L = 100;        % [s]
tstim = L+ttran;      % [s]
tend = tstim + tpre + tpost;
t = 0:dt:tend;
V = zeros(n_trials,length(t));
idx = find(t>tpre+ttran & t<=tpre+tstim);
t = t(idx) - (tpre + ttran);

spike_times = cell(n_voltages,1);
firing_rates = cell(n_voltages,1);
y = cell(n_voltages,1);
bin_width = tarp;

%%% Simulate the neuron
both_correlated = 1;    % if 0, only excitation is correlated, if 1 both exc. and inh. are
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));
stream = RandStream('mt19937ar','Seed',5061983);
seed_exc_c = stream.randi(10000,n_trials,1);
if both_correlated
    seed_inh_c = stream.randi(20000,n_trials,1);
end

for i=1:n_voltages
    fprintf(1, 'Voltage [%03d/%03d]:\n   ', i, n_voltages);
    ratio = computeRatesRatio(mode,V_bal(i),Rm,tau_exc*1e3,tau_inh*1e3,E_exc,E_inh);
    for j=1:n_trials
        fprintf(1, '.');
        if mod(j,50) == 0
            fprintf(1, '\n');
            if j ~= n_trials
                fprintf(1, '   ');
            end
        end
        if both_correlated
            [Gm_exc_c,Gm_inh_c,Gs_exc_c,Gs_inh_c] = ...
                computeSynapticBackgroundCoefficients(mode,Rm,R_exc*c,ratio,tau_exc*1e3,tau_inh*1e3);
            [Gm_exc,Gm_inh,Gs_exc,Gs_inh] = ...
                computeSynapticBackgroundCoefficients(mode,Rm,R_exc*(1-c),ratio,tau_exc*1e3,tau_inh*1e3);
            Ge_c = [pre, OU_fast(tstim,dt,Gm_exc_c,Gs_exc_c,tau_exc,Gm_exc_c,seed_exc_c(j)), post];
            Gi_c = [pre, OU_fast(tstim,dt,Gm_inh_c,Gs_inh_c,tau_inh,Gm_inh_c,seed_inh_c(j)), post];
            Ge = [pre, OU_fast(tstim,dt,Gm_exc,Gs_exc,tau_exc,Gm_exc,randi(10000)), post];
            Gi = [pre, OU_fast(tstim,dt,Gm_inh,Gs_inh,tau_inh,Gm_inh,randi(10000)), post];
            V(j,:) = LIF_fast(tend,dt,C,tau,tarp,Er,E0,Vth,0,Ge_c,E_exc,Gi_c,E_inh,Ge,E_exc,Gi,E_inh);
        else
            [Gm_exc_c,~,Gs_exc_c] = ...
                computeSynapticBackgroundCoefficients(mode,Rm,R_exc*c,ratio,tau_exc*1e3,tau_inh*1e3);
            [Gm_exc,~,Gs_exc] = ...
                computeSynapticBackgroundCoefficients(mode,Rm,R_exc*(1-c),ratio,tau_exc*1e3,tau_inh*1e3);
            [~,Gm_inh,~,Gs_inh] = ...
                computeSynapticBackgroundCoefficients(mode,Rm,R_exc,ratio,tau_exc*1e3,tau_inh*1e3);
            Ge_c = [pre, OU_fast(tstim,dt,Gm_exc_c,Gs_exc_c,tau_exc,Gm_exc_c,seed_exc_c(j)), post];
            Ge = [pre, OU_fast(tstim,dt,Gm_exc,Gs_exc,tau_exc,Gm_exc,randi(10000)), post];
            Gi = [pre, OU_fast(tstim,dt,Gm_inh,Gs_inh,tau_inh,Gm_inh,randi(10000)), post];
            V(j,:) = LIF_fast(tend,dt,C,tau,tarp,Er,E0,Vth,0,Ge_c,E_exc,Ge,E_exc,Gi,E_inh);
        end
    end
    if mod(j,50)
        fprintf('\n');
    end
    spike_times{i} = extractAPPeak(t,V(:,idx),0);
    firing_rates{i} = cellfun(@(x) length(x)/L, spike_times{i});
    y{i} = binarizeSpikeTrain(spike_times{i},bin_width,[0,L]);
end
nu = cellfun(@(x) mean(x), firing_rates);

clear V Ge Gi Ge_c Gi_c i j t pre post ratio Gm_* Gs_* idx stream

%%
% delta = 10*bin_width : bin_width : 120*bin_width;
% rho = zeros(size(delta));
% c_ij = zeros(size(delta));
% c_ii = zeros(size(delta));
% c_jj = zeros(size(delta));
% for i=1:length(delta)
%     fprintf(1, '+');
%     [rho(i),c_ij(i),c_ii(i),c_jj(i)] = ...
%         computeRho(y{1},y{2},L,bin_width,delta(i),firing_rates{1},firing_rates{2});
% end
% 
% plot(delta,rho,'k-o');

%%
geom_mean_rate = [];
r = [];
c_ii = zeros(n_voltages,1);
delta = 70*bin_width;
fprintf(1, 'Computing autocorrelations:\n   ');
for i=1:n_voltages
    fprintf(1,'+');
    [~,~,c_ii(i)] = computeRho(y{i},y{i},L,bin_width,delta,firing_rates{i},firing_rates{i});
end
fprintf(1,'\n');
cnt = 1;
fprintf(1, 'Computing rho:\n   ');
for i=1:n_voltages
    for j=i+1:n_voltages
        fprintf(1,'.');
        if mod(cnt,100) == 0
            fprintf(1,'\n   '); 
        end
        if nu(i)/nu(j) >= 0.5 && nu(i)/nu(j) <= 2
            geom_mean_rate = [geom_mean_rate; sqrt(prod(nu([i,j])))];
            r = [r; computeRho(y{i},y{j},L,bin_width,delta,firing_rates{i},...
                firing_rates{j},c_ii(i),c_ii(j))];
        end
        cnt = cnt+1;
    end
end
fprintf(1,'\n');

clear i j cnt
save(sprintf('LIF_correlations_%s.mat',datestr(now,'yyyymmddHHMMSS')));

% %%
% figure;
% hold on;
% plot(geom_mean_rate,voltage_corr,'ko');
% plot(geom_mean_rate,current_corr,'ro');
% plot(geom_mean_rate,spike_count_corr,'s','Color',[1,.5,0]);
% axis([0,25,0,0.5]);
% hndl = legend('V_m correlation','I correlation','\rho_{ij}','Location','SouthEast');
% set(hndl,'box','off');
% xlabel('Geometric mean rate (Hz)');
% ylabel('Correlation');
% set(gca,'XTick',0:5:20,'YTick',0:.1:.5);
% set(gcf,'Color','w','PaperPosition',[0,0,8,5],'PaperUnits','Inch');
% print('-depsc2','LIF_correlations_instantaneous_decay.eps');
% 
% %%
% window_duration = 1;      % [s]
% window_overlap = 0.5;     % [s]
% window_duration_samples = round(window_duration/dt);
% window_overlap_samples = round(window_overlap/dt);
% 
% ndx = 1:window_duration_samples;
% offset = 0;
% 
% T = window_duration/2 : window_overlap : t(end)-window_duration/2;
% C = zeros(size(T));
% i = 1;
% for offset=0:window_overlap_samples:length(I)-window_duration_samples
%     x = I(:,offset+ndx);
%     C(i) = corr(I(1,offset+ndx)',I(2,offset+ndx)');
%     i = i+1;
% end
% 
% %%
% spks = extractAPPeak(t,V,0);
% isi = cellfun(@(x) diff(x), spks, 'UniformOutput', 0);
% F = cellfun(@(x) 1./x, isi, 'UniformOutput', 0);
% X = cell(n_voltages,1);
% binned_F = cell(n_voltages,1);
% for i=1:n_voltages
%     j = 1;
%     for offset=0:window_overlap_samples:length(I)-window_duration_samples
%         idx = find(spks{i} > t(offset+ndx(1)) & spks{i} < t(offset+ndx(end)));
%         binned_F{i}(j) = length(idx) / window_duration;
%         j = j+1;
%     end
% end
% 
% %%
% window = [10e-3,10e-3];
% displacement = [-0.1 : 0.01 : 0.1, -0.03 : 0.002 : 0.03];
% displacement = sort(unique(displacement));
% n_disp = length(displacement);
% FF = zeros(size(displacement));
% for i=1:n_disp
%     fprintf(1, '[%02d/%02d]\n', i, n_disp);
%     [~,Vwndw] = ETSA(t,V(2,:),spks{1}(2:end-1)+displacement(i),window);
%     Twndw = (0:size(Vwndw,2)-1) * dt;
%     spks_window = extractAPPeak(Twndw,Vwndw,0);
%     FF(i) = sum(cellfun(@(x) length(x), spks_window)) / (length(spks_window)*sum(window));
% end
% %%
% plot(displacement, FF-length(spks{2})/t(end), 'k-o');
% 
% %%
% figure;
% hold on;
% plot(t,V(1,:),'k');
% % plot(T,binned_F{1},'m');
% plot(T,C*100,'b');
