clear all;
close all;
clc;

%%
pars = make_neuron_model_pars('aEIF');
V_bal = -60:4:-20;
mode = 'relative';
tau_exc = 5e-3;
tau_inh = 10e-3;
E_exc = 0;
E_inh = -80;
R_exc = 7000;
R_inh = 2000;
n = length(V_bal);
tau_fixed_inh = zeros(n,1);
tau_fixed_exc = zeros(n,1);
R_inh_fixed_exc = zeros(n,1);
R_exc_fixed_inh = zeros(n,1);
for i=1:n
    ratio = computeRatesRatio(mode,V_bal(i),pars.Rm,tau_exc*1e3,tau_inh*1e3,E_exc,E_inh);
    
    R_exc_fixed_inh(i) = R_inh*ratio;
    [Gm_exc,Gm_inh] = computeSynapticBackgroundCoefficients(mode,...
        pars.Rm,R_exc_fixed_inh(i),ratio,tau_exc*1e3,tau_inh*1e3);
    tau_fixed_inh(i) = pars.C/(pars.gL+Gm_exc+Gm_inh);

    R_inh_fixed_exc(i) = R_exc/ratio;
    [Gm_exc,Gm_inh] = computeSynapticBackgroundCoefficients(mode,...
        pars.Rm,R_exc,ratio,tau_exc*1e3,tau_inh*1e3);
    tau_fixed_exc(i) = pars.C/(pars.gL+Gm_exc+Gm_inh);
end

figure;
subplot(2,1,1);
hold on;
plot(V_bal,tau_fixed_inh,'ko-','MarkerFaceColor','w');
plot(V_bal,tau_fixed_exc,'ro-','MarkerFaceColor','w');
plot(V_bal([1,end])+[-1,1],pars.C/pars.gL+[0,0],'--','LineWidth',2,'Color',[.5,.5,.5]);
set(legend('Effective w/ fixed Ri','Effective w/ fixed Re','Reference',...
    'Location','Best'),'Box','Off');
ylabel('\tau (ms)');
set(gca,'TickDir','Out','XTick',V_bal,'YTick',0:2:10);
axis([V_bal([1,end])+[-1,1],0,10]);

subplot(2,1,2);
hold on;
hndl = zeros(4,1);
hndl(3) = plot(V_bal([1,end])+[-1,1],R_exc*1e-3+[0,0],'r--','LineWidth',2);
hndl(4) = plot(V_bal([1,end])+[-1,1],R_inh*1e-3+[0,0],'b--','LineWidth',2);
hndl(1) = plot(V_bal,R_inh_fixed_exc*1e-3,'bo-','MarkerFaceColor','w');
hndl(2) = plot(V_bal,R_exc_fixed_inh*1e-3,'ro-','MarkerFaceColor','w');
set(gca,'TickDir','Out','XTick',V_bal,'YScale','Log');
set(legend(hndl, {'Inhibitory rate','Excitatory rate','Ref. exc.','Ref. inh.'},...
    'Location','SouthWest'),'Box','Off');
axis([V_bal([1,end])+[-1,1],0.2,40]);
xlabel('Balanced voltage (mV)');
ylabel('Pre-synaptic firing rate (kHz)');

set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,10,5],'PaperSize',[10,5]);
print('-dpdf','tau_with_bg.pdf');
