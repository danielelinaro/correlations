clear all;
close all;
clc;

%%
% Simulation parameters
srate = 20000;        %[Hz]
dt = 1e3/srate;       %[ms]
t_max = 10000/dt;      % duration of the simulation in units of timesteps
time = (0:dt:(t_max-1)*dt)';

% Bi-exponential function
exp2_func = @(param,x) param(1)*exp(-x/param(2)) + param(3)*exp(-x/param(4));

% Model parameters
C = 0.3;          % [uF]
g_l = 0.03;       % [nS]
E_l = -65;        % [mV]
E_reset = -75;    % [mV]
t_refr = 2;       % [ms]
v0 = -45;         % [mV]
DeltaV = 1e-5;    % [mV]
param = [C, g_l, E_l, E_reset, t_refr, v0, DeltaV];

%% Step of current
% Injected current
mu_i = 0.65;      % [nA]
tpre = 300;       % [ms]
tpost = 300;      % [ms]
I = [zeros((tpre/dt),1) ; mu_i + zeros(t_max-((tpre+tpost)/dt),1) ; zeros((tpost/dt),1)];

% Spike-triggered current parameters
eta1 = 0.05;      % [nA]
eta_tau1 = 200;   % [ms]
eta2 = 0.1;       % [nA]
eta_tau2 = 20;    % [ms]
eta = exp2_func([eta1, eta_tau1, eta2, eta_tau2], time);

% Moving threshold parameters
gamma1 = 5;       % [mV]
gamma_tau1 = 150; % [ms]
gamma2 = 10;      % [mV]
gamma_tau2 = 45;  % [ms]
gamma = exp2_func([gamma1, gamma_tau1, gamma2, gamma_tau2], time);

% Membrane voltage
V = cell(2,1);
w = cell(2,1);
Vt = cell(2,1);
tic
[V{1},~,w{1},Vt{1}] = IF_eta(I, param, eta, gamma, 1, srate);
toc
tic
[V{2},w{2},Vt{2}] = IF_eta_fast(t_max*dt*1e-3, dt*1e-3, C*1e-3, g_l, E_l, E_reset, t_refr*1e-3, v0, DeltaV, eta, gamma, I);
toc

figure;
subplot(2,1,1);
hold on;
plot(time*1e-3,V{1},'k');
plot(time*1e-3,V{2},'r');
plot(time*1e-3,v0+Vt{1},'k--');
plot(time*1e-3,v0+Vt{2},'r--');
ylabel('Vm (mV)');
subplot(2,1,2);
hold on;
plot(time*1e-3,-w{1}*1e3,'k');
plot(time*1e-3,-w{2}*1e3,'r');
xlabel('Time (s)');
ylabel('I (pA)');
box off;

%% Sinusoidally-modulated noisy current

% Injected current
mu_i = 0.6+0.6*sin(0.001*time(1:t_max-(100/dt))); % mean of the input current;
sigma_i = 3.5;                                    % standard deviation of the input current
I = mu_i+sigma_i*randn(t_max-(100/dt),1);         % Gaussian input current
I = smooth([zeros((50/dt),1) ; I ; zeros((50/dt),1)],50); %filter the white noise input current to produce colored noise

% Spike-triggered current parameters
eta1 = 0.1;       % [pA]
eta_tau1 = 200;   % [ms]
eta2 = 0.2;       % [pA]
eta_tau2 = 20;    % [ms]
eta = exp2_func([eta1, eta_tau1, eta2, eta_tau2], time);

% Moving threshold parameters
gamma1 = 5;       % [mV]
gamma_tau1 = 150; % [ms]
gamma2 = 10;      % [mV]
gamma_tau2 = 45;  % [ms]
gamma = exp2_func([gamma1, gamma_tau1, gamma2, gamma_tau2], time);

% Membrane voltage
tic
[V{1},~,w{1},Vt{1}] = IF_eta(I, param, eta, gamma, 1, srate);
toc
tic
[V{2},w{2},Vt{2}] = IF_eta_fast(t_max*dt, dt, C, g_l, E_l, E_reset, t_refr, v0, DeltaV, eta, gamma, I);
toc

figure;
subplot(2,1,1);
hold on;
plot(time*1e-3,V{1},'k');
plot(time*1e-3,V{2},'r');
plot(time*1e-3,v0+Vt{1},'k--');
plot(time*1e-3,v0+Vt{2},'r--');
ylabel('Vm (mV)');
subplot(2,1,2);
hold on;
plot(time*1e-3,-w{1}*1e3,'k');
plot(time*1e-3,-w{2}*1e3,'r');
xlabel('Time (s)');
ylabel('I (pA)');
box off;
