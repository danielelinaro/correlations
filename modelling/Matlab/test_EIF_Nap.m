clear all;
close all;
clc;
addpath ~/Postdoc/Researc/MatlabLibrary/neuron_models/

%%
C = 281;        % [pF] membrane capacitance
gL = 30;        % [nS] leak conductance
EL = -70.6;     % [mV] leak reversal potential
VT = -50.4;     % [mV] spike threshold
deltaT = 2;     % [mV] slope factor
Vre = -55;      % [mV] reset voltage
tau_ref = 2e-3; % [s]  refractory period
Iext = 570;     % [pA]
g_naf = 400;
d_nap = 0.7;
g_nap = d_nap * 0.0032 * g_naf; % [mS/cm^2]
% gNap = 0.873246;% [mS/cm^2]

tstim = 1;      % [s]
tpre = 0.1;     % [s]
tpost = 0.2;    % [s]
dt = 1e-5;      % [s]
tend = tstim+tpre+tpost;
t = 0:dt:tend;
% I = OU(tend,dt,400,200,20e-3,500,5061983);
I = 510 + zeros(size(t));
I(t<tpre | t>tpre+tstim) = 0;

V = zeros(2,length(t));
x = zeros(2,length(t));
[V(1,:),x(1,:)] = EIF_Nap_fast(tend,dt,C,g_nap,gL,EL,deltaT,VT,Vre,tau_ref,I);
[V(2,:),x(2,:)] = EIF_Nap_fast(tend,dt,C,0,gL,EL,deltaT,VT,Vre,tau_ref,I);
% [tp,Vp] = extractAPPeak(t,V,0);

figure;
hold on;
plot(t,V(1,:),'k');
plot(t,V(2,:),'r');

%%
orange = [1,0.5,0];
figure;
axes('Position',[0.1,0.925,0.8,0.05],'NextPlot','Add');
plot(t,I,'k');
axis([tpre-0.1,tstim+tpre+0.1,1.2*min(I),1.2*max(I)]);
axis off;

axes('Position',[0.1,0.6,0.8,0.3],'NextPlot','Add');
plot(t,V,'k');
plot(tp{1},Vp{1},'o','Color',orange,'MarkerFaceColor',orange,'MarkerSize',3);
plot(t([1,end]),VT+[0,0],'r--');
axis([tpre-0.1,tstim+tpre+0.1,-80,30]);
ylabel('V (mV)');

axes('Position',[0.1,0.4,0.8,0.1],'NextPlot','Add');
plot(t,x,'k');
axis([tpre-0.1,tstim+tpre+0.1,0,1.2*max(x)]);
xlabel('Time (s)');
ylabel('w (pA)');

axes('Position',[0.1,0.1,0.8,0.2],'NextPlot','Add');
plot(1./diff(tp{1}),'o','Color',orange,'MarkerFaceColor',orange,'MarkerSize',3);
xlabel('Spike #');
ylabel('Instantaneous frequency');

set(gcf,'Color','w');
