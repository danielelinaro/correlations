clear all;
close all;
clc;
% addpath ~/MatlabLibrary;
% addpath ~/MatlabLibrary/neuron_models;

%%
% Mono- and bi-exponential functions
exp_func = @(param,x) param(1)*exp(-x/param(2));
exp2_func = @(param,x) param(1)*exp(-x/param(2)) + param(3)*exp(-x/param(4));

% Model parameters
C = 0.0003;       % [?F]
gl = 0.03;        % [uS]
El = -65;         % [mV]
Er = -55;         % [mV]
tarp = 2e-3;      % [ms]
V0 = -45;         % [mV]
deltaV = 1;       % [mV]

% Spike-triggered current parameters
eta1 = 0.05;      % [nA]
eta_tau1 = 0.2;   % [s]
eta2 = 0.1;       % [nA]
eta_tau2 = 0.02;  % [s]

% Moving threshold parameters
% double exponential model
% gamma1 = 0.5;       % [mV]
% gamma_tau1 = 0.15;  % [s]
% gamma2 = 1;         % [mV]
% gamma_tau2 = 0.045; % [s]
% single exponential model
gamma1 = 5;          % [mV]
gamma_tau1 = 0.02;   % [s]
Im = 0.1:0.2:2.5;
Is = logspace(log10(0.1),log10(3),20);
Itau = 5e-3;       % [s]
c = 0.5;

tmp = repmat(Is(:)',[length(Im),1]);
pars = [repmat(Im(:),[length(Is),1]), tmp(:)];
n_pars = size(pars,1);

%%% Parameters of the simulation
n_trials = 1;
dt = 50e-6;     % [s] = 20 kHz sampling frequency
<<<<<<< HEAD
tpre = 0.5;     % [s]
tpost = 0.1;    % [s]
ttran = 1;      % [s]
L = 50;        % [s]
=======
tpre = 0.2;     % [s]
tpost = 0.2;    % [s]
ttran = 0.2;    % [s]
L = 20;         % [s]
>>>>>>> b9e75dafa1e8c555c7b276ea587107677abaef75
tstim = L+ttran;
tend = tstim + tpre + tpost;
t = 0:dt:tend;
eta = exp2_func([eta1, eta_tau1, eta2, eta_tau2], t);
% gamma = exp2_func([gamma1, gamma_tau1, gamma2, gamma_tau2], t);
gamma = exp_func([gamma1, gamma_tau1], t);
V = zeros(n_trials,length(t));
idx = find(t>tpre+ttran & t<=tpre+tstim);
t = (t(idx) - (tpre + ttran));

spike_times = cell(n_pars,1);
firing_rates = cell(n_pars,1);
y = cell(n_pars,1);
bin_width = 2e-3;

threshold = 0;

%%% Simulate the neuron
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));
stream = RandStream('mt19937ar','Seed',5061983);
seeds = stream.randi(10000,n_trials,1);

for i=1:n_pars
    fprintf(1, '[%03d/%03d] mu = %f, sigma = %f:\n   ', i, n_pars, pars(i,1), pars(i,2));
    for j=1:n_trials
        fprintf(1, '.');
        if mod(j,50) == 0
            fprintf(1, '\n');
            if j ~= n_trials
                fprintf(1, '   ');
            end
        end
        I = [pre, pars(i,1) + pars(i,2) * ...
            (sqrt(1-c) * OU_fast(tstim,dt,0,1,Itau,0,randi(10000)) + ...
            sqrt(c) * OU_fast(tstim,dt,0,1,Itau,0,seeds(j))), post];
        V(j,:) = IF_eta_fast(tend,dt,C,gl,El,Er,tarp,V0,deltaV,eta,gamma,I);
    end
    if mod(j,50)
        fprintf('\n');
    end
    spike_times{i} = extractAPPeak(t,V(:,idx),threshold);
    firing_rates{i} = cellfun(@(x) length(x)/L, spike_times{i});
    y{i} = binarizeSpikeTrain(spike_times{i},bin_width,[0,L]);
end
nu = cellfun(@(x) mean(x), firing_rates);
keyboard
clear V Ge Gi Ge_c Gi_c i j t pre post ratio Gm_* Gs_* idx stream I tmp
fprintf(1, 'Saving data so far... ');
save(['IF_eta_correlations_cc_',suffix,'_data.mat']);
fprintf(1, 'done.\n');

%%
geom_mean_rate = [];
r = [];
c_ii = zeros(n_pars,1);
delta = 30*bin_width;
fprintf(1, 'Computing autocorrelations:\n   ');
for i=1:n_pars
    fprintf(1,'+');
    [~,~,c_ii(i)] = computeRho(y{i},y{i},L,bin_width,delta,firing_rates{i},firing_rates{i});
end
fprintf(1,'\n');
cnt = 1;
fprintf(1, 'Computing rho:\n   ');
for i=1:n_pars
    for j=i+1:n_pars
        fprintf(1,'.');
        if mod(cnt,100) == 0
            fprintf(1,'\n   '); 
        end
        if nu(i)/nu(j) >= 0.5 && nu(i)/nu(j) <= 2
            geom_mean_rate = [geom_mean_rate; sqrt(prod(nu([i,j])))];
            r = [r; computeRho(y{i},y{j},L,bin_width,delta,firing_rates{i},...
                firing_rates{j},c_ii(i),c_ii(j))];
        end
        cnt = cnt+1;
    end
end
fprintf(1,'\n');

clear i j cnt
suffix = datestr(now,'yyyymmddHHMMSS');
fprintf(1, 'Saving everything... ');
save(['IF_eta_correlations_cc_',suffix,'.mat']);
fprintf(1, 'done.\n');

%%
printf(1, 'Generating figure... ');
idx = find(~imag(r));
geom_mean_rate = geom_mean_rate(idx);
r = r(idx);
edges = (1:3:110)';
n = histc(geom_mean_rate,edges);
rm = zeros(length(edges)-1,1);
rs = zeros(length(edges)-1,1);
for i=1:length(edges)-1
    idx = find(geom_mean_rate>=edges(i) & geom_mean_rate<edges(i+1));
    rm(i) = mean(r(idx));
    rs(i) = std(r(idx));
end
edges = edges(1:end-1) + diff(edges)/2;

figure;
hold on;
plot(geom_mean_rate,r,'o','Color',[.6,.6,.6],'MarkerFaceColor',[.6,.6,.6],...
    'MarkerSize',4);
errorbar(edges,rm,rs,'ko','MarkerFaceColor','k','MarkerSize',6);
axis([0,100,0,0.5]);
set(gca,'XTick',0:20:100,'YTick',0:.1:.5);
xlabel('Geometric mean rate');
ylabel('Correlation');
set(gcf,'PaperUnits','Inch','PaperPosition',[0,0,8,5]);
print('-depsc2',['IF_eta_correlations_cc_',suffix,'.eps']);
fprintf(1, 'done.\n');


