clear all;
close all;
clc;
load('EIF_Kv7_correlations_20140314161949.mat','spike_times','bin_width','L','T');

%%
% Ni = n{1};
% Nj = n{2};
Ri = length(spike_times{1});
Rj = length(spike_times{2});
Ni = cell(Ri,1);
Nj = cell(Rj,1);
for i=1:Ri
    [~,~,Ni{i}] = binarizeSpikeTrain(spike_times{1}{i},bin_width,[0,L],T,bin_width);
end
for j=1:Rj
    [~,~,Nj{j}] = binarizeSpikeTrain(spike_times{2}{j},bin_width,[0,L],T,bin_width);
end

%%
cov_ij = zeros(Ri,Rj);
for i=1:Ri
    for j=1:Rj
        cov_ij(i,j) = computeSpikeCountsCovariance(Ni(1:i),Nj(1:j),L,T);
    end
end

var_ii = zeros(Ri,1);
for i=1:Ri
    var_ii(i) = computeSpikeCountsCovariance(Ni(1:i),Ni(1:i),L,T);
end

var_jj = zeros(Rj,1);
for j=1:Rj
    var_jj(j) = computeSpikeCountsCovariance(Nj(1:j),Nj(1:j),L,T);
end

%%
rho = zeros(Ri,Rj);
rho_single = zeros(Ri,Rj);
for i=1:Ri
    for j=1:Rj
        fprintf(1, '.');
        rho(i,j) = computeCorrelationCoefficient(Ni(1:i),Nj(1:j),L,T);
        rho_single(i,j) = computeCorrelationCoefficient(Ni(i),Nj(j),L,T);
    end
end
fprintf(1, '\n');

%%
cmap = gray(64);
cmap(:,1) = 1;
contourf(rho);
% colormap(cmap);
caxis([min(min(rho)),max(max(rho))]);
colorbar;
shading flat;
set(gca,'Layer','Top','Box','On');

%%
% [x,y] = meshgrid(1:Ri,1:Rj);
% figure;
% hold on;
% plot3(x(:),y(:),rho_single(:),'ko');
% plot3(x(:),y(:),rho(:),'rs');
% grid on;
% box on;

%%
figure;
hold on;
rasterplot(spike_times{1}{end},'k');
rasterplot(spike_times{2}{end-1},'r');
