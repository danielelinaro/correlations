clear all;
close all;
clc;

%%
files = {'aLIF_correlations_20140717005930.mat',...
         'aLIF_correlations_cc_20140717122534.mat',...
         'aLIF_correlations_20140717050939.mat',...
         'aLIF_correlations_cc_20140717182322.mat',...
         'aLIF_correlations_20140717031248.mat',...
         };
labels = {'GC','CC','GC','CC','GC'};
nfiles = length(files);
edges = 0:2:30;
nedges = length(edges);
data = cellfun(@(x) load(x, 'geom_mean_rate','r','b'), files);
x = edges(1:end-1) + diff(edges)/2;

for i=1:nfiles
    data(i).n = histc(data(i).geom_mean_rate,edges);
    data(i).rm = zeros(length(edges)-1,1);
    data(i).rs = zeros(length(edges)-1,1);
    for j=1:length(edges)-1
        idx = find(data(i).geom_mean_rate>=edges(j) & data(i).geom_mean_rate<edges(j+1));
        data(i).rm(j) = mean(data(i).r(idx));
        data(i).rs(j) = std(data(i).r(idx));% / sqrt(length(idx));
    end
end

col = [1,.5,0 ; 0,1,.5 ; 1,0,0.5 ; 0,.5,1 ; 0,0,0];
black = [0,0,0];
white = [1,1,1];
light = [.5,.5,.5];

figure;
hold on;
set(gca,'TickDir','Out','FontName','Arial','FontSize',10);
max_rate = 28;
hndl = zeros(nfiles,1);
lgnd = cell(nfiles,1);
for i=1:nfiles
    idx = find(data(i).geom_mean_rate <= max_rate);
    plot(data(i).geom_mean_rate(idx),data(i).r(idx),'o','Color',min(col(i,:)+light,white),...
        'MarkerFaceColor',min(col(i,:)+light,white),'MarkerSize',3);
    idx = find(x <= max_rate);
    hndl(i) = errorbar(x(idx),data(i).rm(idx),data(i).rs(idx),'s-','Color',col(i,:),'LineWidth',2,...
        'MarkerSize',6,'MarkerFaceColor',col(i,:));
    lgnd{i} = sprintf('%s - b = %d pA', labels{i}, data(i).b);
end
axis([0,30,0,0.5]);
set(gca,'XTick',0:5:30,'YTick',0:.1:.5);
xlabel('Geometric mean firing rate (spikes/s)');
ylabel('Current correlation');
h = legend(hndl, lgnd, 'Location','SouthEast');
set(h,'Box','Off');

set(gcf,'Color','White','PaperUnits','Inch','PaperPosition',[0,0,8,7]);
print('-depsc2','aLIF_summary.eps');
print('-dpng','-r300','aLIF_summary.png');
